/* eslint-env browser */
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import 'bootstrap/dist/css/bootstrap.css';

import createApplicationAsync from '../shared';

import browserSiteApi from './api';

let applicationPromise;
const appElement = document.getElementById('app')!;

refreshApplicationAsync();

if (module.hot) {
	module.hot.accept('../shared', refreshApplicationAsync);
}

function createAppAsync(initialState) {
	return createApplicationAsync(initialState, browserSiteApi, BrowserRouter, null);
}

async function refreshApplicationAsync() {
	try {
		const isRunning = !!applicationPromise;
		applicationPromise = isRunning ?
			applicationPromise
				.then(({ serializeStateAsync }) => serializeStateAsync())
				.then(createAppAsync) :
			createAppAsync(readInitialState());

		if (isRunning) {
			ReactDOM.unmountComponentAtNode(appElement);
		}

		ReactDOM.render((await applicationPromise).getView(), appElement);
	} catch (error) {
		console.error(error && (error.stack || error));
	}
}

function readInitialState() {
	const stateElement = document.getElementById('initial-state')!;
	const result = JSON.parse(stateElement.innerHTML);

	stateElement.parentElement!.removeChild(stateElement);

	return result;
}
