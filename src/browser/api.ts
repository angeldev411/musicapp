import * as request from 'request';

import {
	GET,
	POST,
	DELETE,
} from '../shared/constants';
import { ISiteApi } from '../shared/interfaces';
import generateApi from '../shared/api';

const fetch = async (url: string, method: string, body?: object, errorMessage : string = 'Internal server error has occurred.') => {
	const requestInit: RequestInit = {
		credentials: 'include',
		method,
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
	};

	if (body) {
		requestInit.body = JSON.stringify(body);
	}

	const response = await window.fetch(url, requestInit);
	if (!response.ok) {
		throw new Error(errorMessage);
	}
	return await response.json();
};

export default {
	...generateApi(fetch, { music: '/api/music/', accounts: '/api/accounts/' }),
	signInAsync: ({ email, password }) => fetch('/auth/signin', POST, {
		email,
		password,
	}, 'Invalid email or password.'),
	registerAsync: ({ name, email, password }) => fetch('/auth/register', POST, {
		name,
		email,
		password,
	}, 'Failed to register.'),
	forgotPasswordAsync: ({ email }) => fetch('/auth/forgotpassword', POST, {
		email,
	}, 'Failed to request password reset.'),
	signOutAsync: () => fetch('/auth/signout', POST, {}, 'Failed to sign out.'),
	setCookie: (cookieName, cookieValue, expiryDays) => {
		const date = new Date();
		date.setTime(date.getTime() + (expiryDays * 24 * 60 * 60 * 1000));
		const expires = `expires=${date.toUTCString()}`;
		document.cookie = `${cookieName}=${cookieValue};${expires};path=/`;
	},
};
