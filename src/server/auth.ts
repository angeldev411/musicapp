import * as express from 'express';
import * as querystring from 'querystring';
import * as httpClient from 'request';
import { OAuth } from 'request/lib/oauth';
import * as bodyParser from 'body-parser';
import * as url from 'url';
import * as qs from 'qs';

const temporaryCredentials = 'temporarytoken';
const authorize = 'authorize';
const accessToken = 'accesstoken';

const defaultBaseUrl = 'https://auth.logos.com/oauth/v1/';

const verifyPath = '/verify';

type authAppParameters = {
	useHttpsCallback: boolean,
	baseUrl: string,
	singleSignOnFailedUrl?: string,
	oauthCredentials: {
		consumerToken: string,
		consumerSecret: string,
	},
	getSession: (request: Express.Request) => {
		getValue: <T extends string | boolean>(key: string) => T,
		setValue: (key: string, value: string | boolean) => void,
		deleteValue: (key: string) => void,
	},
	getAccountsClient: (request: Express.Request) => {
		getCurrentUserAsync: () => Promise<{ id: number }>,
	},
};

export default function createAuthApp({ useHttpsCallback, baseUrl, oauthCredentials, getSession, getAccountsClient, singleSignOnFailedUrl }: authAppParameters) {
	if (!oauthCredentials) {
		throw new Error('OAuth credentials are required.');
	}

	const { consumerToken, consumerSecret } = oauthCredentials;
	if (!consumerToken || !consumerSecret) {
		throw new Error('OAuth credentials are required.');
	}

	if (typeof getSession !== 'function') {
		throw new Error('getSession is required.');
	}

	const app = express();
	const urlPrefix = useHttpsCallback ? 'https://' : 'http://';

	const baseRequest = httpClient.defaults({ baseUrl: baseUrl || defaultBaseUrl });

	function getCurrentUserAsync(request): Promise<{ id: number}> {
		return getAccountsClient(request)
			.getCurrentUserAsync()
			.catch(() => ({ id: -1 }));
	}

	app.post('/signin', bodyParser.json({ inflate: false }));
	app.post('/signin', async (request, response) => {
		try {
			const body = await new Promise<string>(
				(accept, reject) => {
					baseRequest.post(
						{
							url: 'users/signin?allowSession=true',
							headers: {
								'Accept': 'application/json',
								'Content-Type': 'application/json',
							},
							oauth: {
								consumer_key: consumerToken,
								consumer_secret: consumerSecret,
							},
							body: JSON.stringify({ email: request.body.email, password: request.body.password }),
						},
						(error, httpResponse, responseBody) => (error ? reject(error) : accept(responseBody)));
				});

			const { accessCredentials } = JSON.parse(body);

			const { token, secret } = accessCredentials || { token: null, secret: null };
			if (!token || !secret) {
				throw new Error('Invalid email or password.');
			}

			const session = getSession(request);
			session.setValue('oauthToken', token);
			session.setValue('oauthSecret', secret);

			const user = await getCurrentUserAsync(request);

			if (request.headers.accept === 'application/json') {
				response.status(200).send(JSON.stringify(user));
			} else {
				response.redirect(request.query.callback || '/');
			}
		} catch (error) {
			console.error(error && (error.stack || error));
			if (request.headers.accept === 'application/json') {
				response.status(400).send(JSON.stringify({ error: 'Invalid email or password.' }));
			} else {
				response.redirect('/signin');
			}
		}
	});

	app.post('/register', bodyParser.json({ inflate: false }));
	app.post('/register', async (request, response) => {
		const body = await new Promise<string>(
			(accept, reject) => baseRequest.post(
				{
					url: 'users?allowSession=true&version=1&platform=web&application=logos',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json',
						'X-Referrer': `${urlPrefix}${request.headers.host}/`,
						'User-Agent': request.headers['user-agent'],
					},
					oauth: {
						consumer_key: consumerToken,
						consumer_secret: consumerSecret,
					},
					body: JSON.stringify({
						email: request.body.email,
						password: request.body.password,
						name: request.body.name,
					}),
				},
				(error, httpResponse, responseBody) => (error ? reject(error) : accept(responseBody))))
			.catch(error => {
				console.error('Error registering new account', error && (error.stack || error));
				return '{}';
			});

		const { accessCredentials } = JSON.parse(body);
		const { token, secret } = accessCredentials || { token: null, secret: null };

		// TODO: Better error messages. Account services does not currently
		// return specific, machine-readable error codes.
		if (!token || !secret) {
			response.status(400).send(JSON.stringify({ error: 'Error creating a new account.' }));
		}

		const session = getSession(request);
		session.setValue('oauthToken', token);
		session.setValue('oauthSecret', secret);

		const user = await getCurrentUserAsync(request);

		if (request.headers.accept === 'application/json') {
			response.status(200).send(JSON.stringify(user));
		} else {
			response.redirect(request.query.callback || '/');
		}
	});

	app.post('/forgotpassword', bodyParser.json({ inflate: false }));
	app.post('/forgotpassword', async (request, response) => {
		await new Promise(
			(accept, reject) => baseRequest.post(
				{
					// TODO: send password link for the correct brand.
					url: `forgotpassword?brand=logos&email=${encodeURIComponent(request.body.email)}`,
				},
				(error, httpResponse, body) => (error ? reject(error) : accept(body))))
			.catch(error => {
				// TODO: Somehow indicate to the client that password reset failed.
				// There doesn't currently seem to be a way to indicate this to the brand bar component.
				console.error('Error sending forgotten password email', error && (error.stack || error));
				return '{}';
			});

		response.status(200).send('{}');
	});

	app.get('/tokensignin', (request, response) => {
		const session = getSession(request);

		let redirectLocation = request.query.returnUrl || '/';
		if (url.parse(redirectLocation).host) {
			redirectLocation = '/';
		}

		function failTokenSignin() {
			redirectLocation = singleSignOnFailedUrl || redirectLocation;
			if (session.getValue<boolean>('disableSingleSignOn')) {
				response.redirect(redirectLocation);
				return;
			}

			const parsedUrl = url.parse(redirectLocation);
			const query = querystring.parse(parsedUrl.query);
			query.sso = false;
			parsedUrl.search = `?${querystring.stringify(query)}`;

			response.redirect(url.format(parsedUrl));
			return;
		}

		if (!request.query.token) {
			failTokenSignin();
			return;
		}

		baseRequest.post(
			{
				url: `users/autosignin?token=${encodeURIComponent(request.query.token)}`,
				oauth: {
					consumer_key: consumerToken,
					consumer_secret: consumerSecret,
				},
			},
			(error, httpResponse, body) => {
				if (error) {
					console.error('Error performing autosignin', error && (error.stack || error));
					failTokenSignin();
					return;
				}

				const { accessCredentials } = JSON.parse(body);
				if (!accessCredentials) {
					console.error('Error performing autosignin:', body);
					failTokenSignin();
					return;
				}

				session.setValue('oauthToken', accessCredentials.token);
				session.setValue('oauthSecret', accessCredentials.secret);

				response.redirect(redirectLocation);
			});
	});

	app.get('/single-sign-on-cookies.js', (request, response) => {
		const session = getSession(request);

		const oauthToken = session.getValue<string>('oauthToken');
		const oauthSecret = session.getValue<string>('oauthSecret');

		if (!oauthToken || !oauthSecret) {
			return response.status(200).send('');
		}

		const cookiesUrl = `${baseUrl}users/cookies`;
		const urlParts = cookiesUrl.split('//');

		const headerComponents = OAuth.prototype.buildParams(
			{
				consumer_key: consumerToken,
				consumer_secret: consumerSecret,
				token: oauthToken,
				token_secret: oauthSecret,
			},
			{ protocol: urlParts[0], host: urlParts[1], pathname: '' },
			'GET',
			'',
			null,
			qs);

		const authorizationHeader = `OAuth ${OAuth.prototype.concatParams(headerComponents, ',', '"')}`;

		return response.redirect(`${cookiesUrl}?authorizationHeader=${encodeURIComponent(authorizationHeader)}`);
	});

	app.get('/signin', async (request, response, next) => {
		const getCurrentUserRequest = getCurrentUserAsync(request);

		const temporaryCredentialsRequest = new Promise<string>((accept, reject) => {
			const callbackUrl = [ urlPrefix, request.headers.host, request.baseUrl, verifyPath ].join('');
			baseRequest.post(
				{
					url: temporaryCredentials,
					oauth: {
						callback: callbackUrl,
						consumer_key: consumerToken,
						consumer_secret: consumerSecret,
					},
				},
				(error, httpResponse, body) => (error ? reject(error) : accept(body)));
		});

		const callback = request.query.callback;

		try {
			const [ { id }, body ] = await Promise.all([ getCurrentUserRequest, temporaryCredentialsRequest ]);

			const { oauth_token, oauth_token_secret } = querystring.parse(body);

			if (id !== -1) {
				return response.redirect(callback || '/');
			}

			const session = getSession(request);
			session.setValue('temporarySecret', oauth_token_secret);
			session.setValue('callback', callback);

			return response.redirect([ baseUrl, authorize, '?', querystring.stringify({ oauth_token }) ].join(''));
		} catch (error) {
			console.error('Error creating temporary credentials:', error && (error.stack || error));
			return next();
		}
	});

	app.get(verifyPath, (request, response, next) => {
		const session = getSession(request);
		const { oauth_token: temporaryOauthToken, oauth_verifier: oauthVerifier } = request.query;
		const temporarySecret = session.getValue<string>('temporarySecret');

		if (!temporaryOauthToken || !oauthVerifier || !temporarySecret) {
			next();
			return;
		}

		const callback = session.getValue<string>('callback') || '/';
		session.deleteValue('temporarySecret');
		session.deleteValue('callback');

		baseRequest.post(
			{
				url: accessToken,
				oauth: {
					consumer_key: consumerToken,
					consumer_secret: consumerSecret,
					token: temporaryOauthToken,
					token_secret: temporarySecret,
					verifier: oauthVerifier,
				},
			},
			(error, httpResponse, body) => {
				if (error) {
					console.error('Error getting final credentials', error && (error.stack || error));
					next();
					return;
				}

				const { oauth_token: oauthToken, oauth_token_secret: oauthTokenSecret } = querystring.parse(body);
				session.setValue('oauthToken', oauthToken);
				session.setValue('oauthSecret', oauthTokenSecret);

				response.redirect(callback);
			});
	});

	app.post('/signout', (request, response) => {
		const session = getSession(request);

		baseRequest.post(
			{
				url: 'users/signout',
				oauth: {
					token: session.getValue<string>('oauthToken'),
					token_secret: session.getValue<string>('oauthSecret'),
					consumer_key: consumerToken,
					consumer_secret: consumerSecret,
				},
			},
			error => {
				if (error) {
					console.error('Error performing signout', error && (error.stack || error));
					if (request.headers.accept === 'application/json') {
						response.status(500).send(JSON.stringify({ success: false }));
					} else {
						response.redirect(request.query.callback || '/');
					}
				}

				session.deleteValue('oauthToken');
				session.deleteValue('oauthSecret');
				session.setValue('disableSingleSignOn', true);

				if (request.headers.accept === 'application/json') {
					response.status(200).send(JSON.stringify({ success: true }));
				} else {
					response.redirect(request.query.callback || '/');
				}
			});
	});

	return {
		authApp: app,
		getCurrentUserCredentials(request) {
			const session = getSession(request);

			return { oauthToken: session.getValue<string>('oauthToken'), oauthSecret: session.getValue<string>('oauthSecret') };
		},
		createRedirectBasedSingleSignOnMiddleware(authAppPrefix) {
			return (request, response, next) => {
				const session = getSession(request);

				if (request.query.sso === 'false' ||
					(session.getValue('oauthToken') && session.getValue('oauthSecret')) ||
					session.getValue('disableSingleSignOn')) {
					next();
					return;
				}

				session.setValue('disableSingleSignOn', true);

				const returnUrlPrefix = useHttpsCallback ? `https://${request.hostname}` : `http://${request.headers.host}`;

				const returnUrl = `${returnUrlPrefix}${authAppPrefix}/tokensignin?returnUrl=${encodeURIComponent(request.originalUrl)}`;

				const autoSignInBaseUrl = `${baseUrl}users/autosignin`;
				const autoSignInQuery = `returnUrl=${encodeURIComponent(returnUrl)}`;
				const urlParts = autoSignInBaseUrl.split('//');

				const headerComponents = OAuth.prototype.buildParams(
					{
						consumer_key: consumerToken,
						consumer_secret: consumerSecret,
					},
					{ protocol: urlParts[0], host: urlParts[1], pathname: '' },
					'GET',
					autoSignInQuery,
					null,
					qs);

				const authorizationHeader = `OAuth ${OAuth.prototype.concatParams(headerComponents, ',', '"')}`;

				response.redirect(`${autoSignInBaseUrl}?${autoSignInQuery}&authorizationHeader=${encodeURIComponent(authorizationHeader)}`);
			};
		},
	};
}
