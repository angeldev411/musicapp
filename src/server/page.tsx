/* eslint-disable react/no-danger */

import * as React from 'react';
import * as ReactDOMServer from 'react-dom/server';

export default async function renderPageAsync({ getView, serializeStateAsync }) {
	const state = await serializeStateAsync();
	return ReactDOMServer.renderToStaticMarkup(
		<html lang="en-US">
			<head>
				<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
				<link rel="stylesheet" href="/dist/styles.css" />
				<meta httpEquiv="Content-Type" content="text/html; charset=UTF-8" />
				<meta name="viewport" content="width=device-width,initial-scale=1" />
				<meta property="og:type" content="website" />
				<meta property="og:site_name" content="Faithlife Music" />
			</head>
			<body>
				<div id="app" dangerouslySetInnerHTML={{ __html: ReactDOMServer.renderToString(getView()) }} />
				<script id="initial-state" type="application/json" dangerouslySetInnerHTML={{ __html: JSON.stringify(state) }} />
				<script type="text/javascript" src="/dist/index.js" />
			</body>
		</html>
	);
}
