import * as request from 'request';

import { oauthCredentials, baseUrls } from './config';
import {
	GET,
	POST,
	DELETE,
	facets,
	sortDefault,
} from '../shared/constants';
import { ISiteApi, IUserInfo } from '../shared/interfaces';
import generateApi from '../shared/api';

export default function createServerSiteApi(getCurrentUserCredentials: () => { oauthToken: string, oauthSecret: string }) : ISiteApi {
	const createRequestPromise = (url: string, method: string, body: any = null) => {
		const credentials = getCurrentUserCredentials();

		const options = {
			url,
			method,
			body,
			oauth: {
				consumer_key: oauthCredentials.consumerToken,
				consumer_secret: oauthCredentials.consumerSecret,
				token: credentials.oauthToken,
				token_secret: credentials.oauthSecret,
			},
		};

		return new Promise<string>((accept, reject) =>
			request(options,
			(error, response, data) => (error ? reject(error) : accept(data))))
			.then(res => JSON.parse(res));
	};

	//User api
	const getCurrentUserAsync = (() => () => {
		let oauthToken;
		let oauthSecret;
		let currentUserPromise : Promise < IUserInfo > | null = null;

		const credentials = getCurrentUserCredentials();
		if (!credentials.oauthToken || !credentials.oauthSecret) {
			return Promise.resolve({ id: -1 });
		}

		if (!currentUserPromise || oauthToken !== credentials.oauthToken || oauthSecret !== credentials.oauthSecret) {
			oauthToken = credentials.oauthToken;
			oauthSecret = credentials.oauthSecret;

			currentUserPromise = createRequestPromise(`${baseUrls.accounts}users/me`, GET);
		}
		return currentUserPromise;
	})();

	return {
		...generateApi(createRequestPromise, baseUrls),
		getCurrentUserAsync,
		signInAsync: ({ email, password }) => Promise.reject(new Error('signInAsync is unsupported on the server.')),
		registerAsync: ({ name, email, password }) => Promise.reject(new Error('registerAsync is unsupported on the server.')),
		forgotPasswordAsync: ({ email }) => Promise.reject(new Error('forgotPasswordAsync is unsupported on the server.')),
		signOutAsync: () => Promise.reject(new Error('signOutAsync is unsupported on the server.')),
		setCookie: (cookieName, cookieValue, expiryDays) => new Error('setCookie is unsupported on the server.'),
	};
}
