import * as express from 'express';
import * as request from 'request';
import { oauthCredentials } from './config';

const { consumerToken, consumerSecret } = oauthCredentials;

type proxyParameters = {
	baseUrl: string,
	getCurrentUserCredentials: (request: Express.Request) => { oauthToken: string, oauthSecret: string },
};

export default function createProxy({ baseUrl, getCurrentUserCredentials }: proxyParameters) {
	if (typeof baseUrl !== 'string') {
		throw new Error('baseUrl is required');
	}

	if (typeof getCurrentUserCredentials !== 'function') {
		throw new Error('getCurrentUserCredentials is required.');
	}

	const app = express();

	app.use(async (req, res) => {
		const { oauthToken, oauthSecret } = await getCurrentUserCredentials(req) || {} as any;

		const proxyRequest = request({
			baseUrl,
			url: req.url,
			method: req.method,
			oauth: {
				consumer_key: consumerToken,
				consumer_secret: consumerSecret,
				token: oauthToken,
				token_secret: oauthSecret,
			},
		});

		req.pipe(proxyRequest);
		proxyRequest.pipe(res);
	});

	return app;
}
