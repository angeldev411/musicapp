import * as path from 'path';
import app from './app';

const portNumber = 3000;

let checkForUpdateAsync;

if (module.hot) {
	// eslint-disable-next-line global-require
	const logApplyResult = require('webpack/hot/log-apply-result');

	// @types/webpack-env currently declares module.hot.check incorrectly:
	// in a previous version, it accepted a callback, but it now returns a promise.
	const checkAsync : any = module.hot.check;

	checkForUpdateAsync = async () => {
		if (module.hot.status() !== 'idle') {
			return;
		}

		let updatedModules;
		try {
			updatedModules = await checkAsync(true);
		} catch (error) {
			console.error('Failed to apply update', error.stack || error.message);
			if ([ 'abort', 'fail' ].indexOf(module.hot.status()) >= 0) {
				throw error;
			}
		}

		if (!updatedModules) {
			return;
		}

		logApplyResult(updatedModules, updatedModules);
		await checkForUpdateAsync();
	};

	module.hot.accept('./app', () => {
		console.log('reloaded server');
	});
} else {
	checkForUpdateAsync = () => { throw new Error('Hot module replacement is not enabled'); };
}

export async function startAsync(compiler) {
	let server;

	if (compiler) {
		const WebpackDevServer = await import('webpack-dev-server');

		server = new WebpackDevServer(compiler, {
			hot: true,
			publicPath: '/dist/',
			disableHostCheck: true,
			setup: webpackServer => {
				webpackServer.use((request, response, next) => app(request, response, next));
			},
		});
	} else {
		const express = await import('express');

		server = express()
			.use('/dist', express.static(path.join(__dirname, '..', '..', 'dist', 'browser')))
			.use(app);
	}

	const result = server.listen(portNumber);
	console.log('Server is running on port', portNumber);

	return result;
}

export function reloadAsync() {
	return checkForUpdateAsync();
}
