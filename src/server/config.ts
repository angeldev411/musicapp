const oauthEnvCredentials = {
	production: {
		consumerToken: 'C5FDE2AA0E0E65E573503B8EF77729D6284E24E5',
		consumerSecret: '515A59DEBD26B820E3BDF1C631775A7AB5A7B1E1',
	},
	development: {
		consumerToken: '1ADC227FC2822FC3EA98A4DDE10A7D7C95D109A1',
		consumerSecret: 'A0C9DACB5536CDCAE8B8541C16B0B8A3E7A2FA2F',
	},
};

const baseEnvUrls = {
	development: {
		auth: 'https://test.auth.logos.com/oauth/v1/',
		accounts: 'https://test.accountsapi.logos.com/v1/',
		music: 'https://internal.musicapi.faithlife.com/v1/',
		community: 'https://test.api.faithlife.com/community/v1/',
		libraryAutoComplete: 'https://test.libraryautocompleteapi.faithlife.com/v1/',
	},
	production: {
		auth: 'https://auth.logos.com/oauth/v1/',
		accounts: 'https://accountsapi.logos.com/v1/',
		music: 'https://musicapi.faithlife.com/v1/',
		community: 'https://api.faithlife.com/community/v1/',
		libraryAutoComplete: 'https://libraryautocompleteapi.faithlife.com/v1/',
	},
};

export const environment = process.env.NODE_ENV === 'production' ? 'production' : 'development';
export const baseUrls = baseEnvUrls[environment];
export const oauthCredentials = oauthEnvCredentials[environment];
