import * as express from 'express';
import Cookies from 'cookies';
import * as sessions from 'client-sessions';
import * as httpClient from 'request';
import * as React from 'react';
import StaticRouter from 'react-router/StaticRouter';

import createApplicationAsync from '../shared';
import { ISiteApi } from '../shared/interfaces';

import createAuthApp from './auth';
import renderPageAsync from './page';
import { oauthCredentials, environment, baseUrls } from './config';
import createServerSiteApi from './api';
import createProxy from './proxy';

const getLocationElements = (location: string) => {
	const index = location.indexOf('?');

	if (index < 0) {
		return {
			pathname: location,
			search: '',
		};
	}

	return {
		pathname: location.slice(0, index),
		search: location.slice(index),
	};
};

declare global {
	namespace Express {
		export interface Request {
			auth: {
				[key: string]: string | boolean,
			},
			api: ISiteApi,
		}
	}
}
const isProduction = environment === 'production';
const app = express();

// Allow connecting to self-signed environments.
if (!isProduction) {
	process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
}

app.get('/up', (request, response) =>
	response.status(204).send());

app.use(sessions({
	cookieName: 'auth',
	duration: 1000 * 60 * 60 * 24 * 365 * 10,
	secure: isProduction,
	httpOnly: true,
	ephemeral: false,
	secret: 'this is not very secret',
}));

const { authApp, getCurrentUserCredentials, createRedirectBasedSingleSignOnMiddleware } = createAuthApp({
	useHttpsCallback: isProduction,
	baseUrl: baseUrls.auth,
	oauthCredentials,
	getAccountsClient: request => request.api,
	getSession(request) {
		const auth = request.auth;
		return {
			getValue: <T extends string | boolean>(key) => auth[key] as T,
			setValue(key, value) {
				auth[key] = value;
			},
			deleteValue(key) {
				delete auth[key];
			},
		};
	},
});

app.use((request, response, next) => {
	// eslint-disable-next-line no-param-reassign
	request.api = createServerSiteApi(() => getCurrentUserCredentials(request));
	next();
});
app.use(express.static('static'));
app.use('/auth', authApp);

app.use(createRedirectBasedSingleSignOnMiddleware('/auth'));

app.use('/api/accounts', createProxy({ baseUrl: baseUrls.accounts, getCurrentUserCredentials }));
app.use('/api/music', createProxy({ baseUrl: baseUrls.music, getCurrentUserCredentials }));
app.use('/api/community', createProxy({ baseUrl: baseUrls.community, getCurrentUserCredentials }));
app.use('/api/libraryAutoComplete', createProxy({ baseUrl: baseUrls.libraryAutoComplete, getCurrentUserCredentials }));

app.get('*', async (request, response, next) => {
	try {
		const routerContext: any = {};
		const cookies = new Cookies(request, response);
		const initialState = {
			ui: {
				isDetailsPaneVisible: cookies.get('isDetailsPaneVisible') === '1',
			},
		};
		const application = await createApplicationAsync(initialState, request.api, StaticRouter, { location: request.url, context: routerContext });
		const {
			pathname,
			search,
		} = getLocationElements(request.url);
		application.navigate(pathname, search);

		const result = await renderPageAsync(application);

		if (routerContext.status === 404) {
			next();
			return;
		}
		if (routerContext.url) {
			response.status(301).setHeader('Location', routerContext.url);
			response.end();
			return;
		}
		response.status(200).send(result);
	} catch (error) {
		next(error);
	}
});

app.use((error, request, response, next) => {
	console.error('Error rendering page', error && (error.stack || error));
	response.status(500).send('Internal server error');
});

export default app;
