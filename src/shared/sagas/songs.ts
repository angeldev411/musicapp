import { take, takeLatest, fork, put, select } from 'redux-saga/effects';

import { ISiteApi } from '../interfaces';
import {
	fetchSearchResultsActionTypes,
	fetchMoreSearchResultsActionTypes,
	fetchSongDetailsActionTypes,
	fetchSearchFieldsActionTypes,
	updateUserMetadataActionType,
} from '../constants';
import {
	onFetchSearchResultsSucceeded,
	onFetchSearchResultsFailed,
	onFetchMoreSearchResultsSucceeded,
	onFetchMoreSearchResultsFailed,
	onFetchSongDetailsSucceeded,
	onFetchSongDetailsFailed,
	onFetchSearchFieldsSucceeded,
	onFetchSearchFieldsFailed,
	onUpdateUserMetadataSucceeded,
	onUpdateUserMetadataFailed,
} from '../actions';

import {
	selectSongs,
	selectSongBySlug,
} from '../selectors';

import { mapToSong } from '../utils';

export default (api: ISiteApi) => {
	function* searchSongWatcher() {
		yield takeLatest(fetchSearchResultsActionTypes.processing, searchSongWorker);
	}

	function* searchSongWorker(action) {
		try {
			const { query, limit, sort } = action;
			const response = yield api.doSearchAsync(query, limit, sort);
			const filter = {
				query,
				limit,
				sort,
			};
			yield put(onFetchSearchResultsSucceeded(response, filter));
		} catch (error) {
			console.error(error);
			yield put(onFetchMoreSearchResultsFailed(error.message));
		}
	}

	function* getMoreSearchResultsWatcher() {
		for (;;) {
			try {
				yield take(fetchMoreSearchResultsActionTypes.processing);
				const { filter, details: { hits } } = yield select(selectSongs);
				const response = yield api.getMoreResultsAsync(filter.query, hits.length, filter.limit, filter.sort);
				yield put(onFetchMoreSearchResultsSucceeded(response));
			} catch (error) {
				console.error(error);
				yield put(onFetchMoreSearchResultsFailed(error.message));
			}
		}
	}

	function* getSongDetailsWatcher() {
		yield takeLatest(fetchSongDetailsActionTypes.processing, getSongDetailsWorker);
	}

	function* getSongDetailsWorker(action) {
		try {
			const { slug } = action;
			let currentSong = yield select(selectSongBySlug, slug);
			if (!currentSong) {
				const songDetails = yield api.getSongDetailsAsync(slug);
				currentSong = mapToSong(songDetails.song);
			}
			yield put(onFetchSongDetailsSucceeded(currentSong));
		} catch (error) {
			console.error(error);
			yield put(onFetchSongDetailsFailed(error.message));
		}
	}

	function* updateUserMetadataWatcher() {
		yield takeLatest(updateUserMetadataActionType.processing, updateUserMetadataWorker);
	}

	function* updateUserMetadataWorker(action) {
		try {
			const { songId, userData } = action;
			const songDetails = yield api.updateUserDataAsync(songId, { userData });
			const currentSong = mapToSong(songDetails.song);
			yield put(onUpdateUserMetadataSucceeded(songId, currentSong));
		} catch (error) {
			console.error(error);
			yield put(onUpdateUserMetadataFailed(error));
		}
	}

	function* getSearchFieldsWatcher() {
		yield takeLatest(fetchSearchFieldsActionTypes.processing, getSearchFieldsWorker);
	}

	function* getSearchFieldsWorker() {
		try {
			const { fields } = yield api.getSearchFieldsAsync();
			yield put(onFetchSearchFieldsSucceeded(fields));
		} catch (error) {
			yield put(onFetchSearchFieldsFailed(error));
		}
	}

	return function* root() {
		yield [
			fork(searchSongWatcher),
			fork(getMoreSearchResultsWatcher),
			fork(getSongDetailsWatcher),
			fork(updateUserMetadataWatcher),
			fork(getSearchFieldsWatcher),
		];
	};
};
