import { fork } from 'redux-saga/effects';

import { ISiteApi } from '../interfaces';

import sitesSagaGenerator from './sites';
import songsSagaGenerator from './songs';
import userSagaGenerator from './user';
import uiSagaGenerator from './ui';

export default (api: ISiteApi) => {
	const sitesSaga = sitesSagaGenerator(api);
	const songsSaga = songsSagaGenerator(api);
	const userSaga = userSagaGenerator(api);
	const uiSaga = uiSagaGenerator(api);

	return function* root() {
		yield [
			fork(sitesSaga),
			fork(songsSaga),
			fork(userSaga),
			fork(uiSaga),
		];
	};
};
