import { take, put, fork, select, call } from 'redux-saga/effects';

import { ISiteApi } from '../interfaces';
import {
	fetchCurrentUserActionTypes,
	signInActionTypes,
	signOutActionTypes,
	registerActionTypes,
	forgotPasswordActionTypes,
} from '../constants';
import {
	onFetchCurrentUserRequested,
	onFetchCurrentUserSucceeded,
	onFetchCurrentUserFailed,
	onSignInSucceeded,
	onSignInFailed,
	onSignOutSucceeded,
	onSignOutFailed,
	onRegisterSucceeded,
	onRegisterFailed,
	onForgotPasswordSucceeded,
	onForgotPasswordFailed,
} from '../actions';

export default (api: ISiteApi) => {
	const createAuthHandler = (performApiCall, succeededAction, failedAction) =>
		function* tryHandleAuthAction(action) {
			try {
				const response = yield performApiCall(action);
				yield put(succeededAction(response));
			} catch (error) {
				console.error(error);
				yield put(failedAction(error.message));
			}
		};

	const handleFetchCurrentUser = createAuthHandler(
		() => api.getCurrentUserAsync(),
		onFetchCurrentUserSucceeded,
		onFetchCurrentUserFailed);

	const authHandlers = {
		[signInActionTypes.processing]: createAuthHandler(
			({ email, password }) => api.signInAsync({ email, password }),
			onSignInSucceeded,
			onSignInFailed),
		[registerActionTypes.processing]: createAuthHandler(
			({ name, email, password }) => api.registerAsync({ name, email, password }),
			onRegisterSucceeded,
			onRegisterFailed),
		[forgotPasswordActionTypes.processing]: createAuthHandler(
			({ email }) => api.forgotPasswordAsync({ email }),
			onForgotPasswordSucceeded,
			onForgotPasswordFailed),
		[signOutActionTypes.processing]: createAuthHandler(
			() => api.signOutAsync(),
			onSignOutSucceeded,
			onSignOutFailed),
	};

	function* authWatcher() {
		const isInitialized = yield select(({ user }) => user.isInitialized);
		if (!isInitialized) {
			const action = onFetchCurrentUserRequested();
			yield put(action);
			yield call(handleFetchCurrentUser, action);
		}

		for (;;) {
			const userId = yield select(({ user }) => user.details.id);

			const action = userId === -1 ?
				yield take([ signInActionTypes.processing, registerActionTypes.processing, forgotPasswordActionTypes.processing ]) :
				yield take(signOutActionTypes.processing);

			yield call(authHandlers[action.type], action);
		}
	}

	return function* root() {
		yield fork(authWatcher);
	};
};
