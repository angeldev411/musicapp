import { takeLatest, fork, put } from 'redux-saga/effects';

import { ISiteApi } from '../interfaces';
import {
	getSitesActionTypes,
} from '../constants';
import {
	onGetSitesSucceeded,
	onGetSitesFailed,
} from '../actions';

export default (api: ISiteApi) => {
	function* getSitesWatcher() {
		yield takeLatest(getSitesActionTypes.processing, getSitesWorker);
	}

	function* getSitesWorker(action) {
		try {
			const response = yield api.getSitesAsync();

			if (!response.sites || !Array.isArray(response.sites)) {
				throw new Error('Malformed api response.');
			}

			yield put(onGetSitesSucceeded(response.sites));
		} catch (error) {
			console.error(error);
			yield put(onGetSitesFailed(error.message));
		}
	}

	return function* root() {
		yield [
			fork(getSitesWatcher),
		];
	};
};
