import { takeLatest, fork, put } from 'redux-saga/effects';

import { ISiteApi } from '../interfaces';
import {
	updateDetailsPaneVisibilityActionType,
} from '../constants';

export default (api: ISiteApi) => {
	function* updateUIWatcher() {
		yield takeLatest(updateDetailsPaneVisibilityActionType, updateUIWorker);
	}

	function updateUIWorker(action) {
		api.setCookie('isDetailsPaneVisible', action.isVisible ? '1' : '0', 1000);
	}

	return function* root() {
		yield [
			fork(updateUIWatcher),
		];
	};
};
