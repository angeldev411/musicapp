import {
	fetchCurrentUserActionTypes,
	signInActionTypes,
	signOutActionTypes,
	registerActionTypes,
	forgotPasswordActionTypes,
} from '../constants';

export function onFetchCurrentUserRequested() {
	return {
		type: fetchCurrentUserActionTypes.processing,
	};
}

export function onFetchCurrentUserSucceeded(user) {
	return {
		type: fetchCurrentUserActionTypes.success,
		user,
	};
}

export function onFetchCurrentUserFailed(error) {
	return {
		type: fetchCurrentUserActionTypes.failure,
		error,
	};
}

export function onSignInRequested({ email, password }) {
	return {
		type: signInActionTypes.processing,
		email,
		password,
	};
}

export function onSignInSucceeded(user) {
	return {
		type: signInActionTypes.success,
		user,
	};
}

export function onSignInFailed(error) {
	return {
		type: signInActionTypes.failure,
		error,
	};
}

export function onSignOutRequested() {
	return {
		type: signOutActionTypes.processing,
	};
}

export function onSignOutSucceeded() {
	return {
		type: signOutActionTypes.success,
	};
}

export function onSignOutFailed(error) {
	return {
		type: signOutActionTypes.failure,
		error,
	};
}

export function onRegisterRequested({ name, email, password }) {
	return {
		type: registerActionTypes.processing,
		name,
		email,
		password,
	};
}

export function onRegisterSucceeded(user) {
	return {
		type: registerActionTypes.success,
		user,
	};
}

export function onRegisterFailed(error) {
	return {
		type: registerActionTypes.failure,
		error,
	};
}

export function onForgotPasswordRequested({ email }) {
	return {
		type: forgotPasswordActionTypes.processing,
		email,
	};
}

export function onForgotPasswordSucceeded() {
	return {
		type: forgotPasswordActionTypes.success,
	};
}

export function onForgotPasswordFailed(error) {
	return {
		type: forgotPasswordActionTypes.failure,
		error,
	};
}
