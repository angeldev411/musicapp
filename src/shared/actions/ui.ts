import {
	updateDetailsPaneVisibilityActionType,
} from '../constants';

export function onUpdateDetailsPaneVisibilityRequested(isVisible) {
	return {
		type: updateDetailsPaneVisibilityActionType,
		isVisible,
	};
}
