import {
	fetchSearchResultsActionTypes,
	fetchMoreSearchResultsActionTypes,
	fetchSongDetailsActionTypes,
	fetchSearchFieldsActionTypes,
	sortDefault,
	updateFacetsActionType,
	updateUserMetadataActionType,
} from '../constants';

export function onFetchSearchResultsRequested({ query, limit = 25, sort = sortDefault }) {
	return {
		type: fetchSearchResultsActionTypes.processing,
		query,
		limit,
		sort,
	};
}

export function onFetchSearchResultsSucceeded(song, filter) {
	return {
		type: fetchSearchResultsActionTypes.success,
		song,
		filter,
	};
}

export function onFetchSearchResultsFailed(error) {
	return {
		type: fetchSearchResultsActionTypes.failure,
		error,
	};
}


export function onFetchMoreSearchResultsRequested() {
	return {
		type: fetchMoreSearchResultsActionTypes.processing,
	};
}

export function onFetchMoreSearchResultsSucceeded(song) {
	return {
		type: fetchMoreSearchResultsActionTypes.success,
		song,
	};
}

export function onFetchMoreSearchResultsFailed(error) {
	return {
		type: fetchMoreSearchResultsActionTypes.failure,
		error,
	};
}

export function onUpdateFacets(facets) {
	return {
		type: updateFacetsActionType,
		facets,
	};
}

export function onFetchSongDetailsRequested(slug) {
	return {
		type: fetchSongDetailsActionTypes.processing,
		slug,
	};
}

export function onFetchSongDetailsSucceeded(song) {
	return {
		type: fetchSongDetailsActionTypes.success,
		song,
	};
}

export function onFetchSongDetailsFailed(error) {
	return {
		type: fetchSongDetailsActionTypes.failure,
		error,
	};
}

export function onUpdateUserMetadataRequested(songId, userData) {
	return {
		type: updateUserMetadataActionType.processing,
		songId,
		userData,
	};
}

export function onUpdateUserMetadataSucceeded(songId, songDetails) {
	return {
		type: updateUserMetadataActionType.success,
		songId,
		songDetails,
	};
}

export function onUpdateUserMetadataFailed(error) {
	return {
		type: updateUserMetadataActionType.failure,
		error,
	};
}

export function onFetchSearchFieldsRequested() {
	return {
		type: fetchSearchFieldsActionTypes.processing,
	};
}

export function onFetchSearchFieldsSucceeded(fields) {
	return {
		type: fetchSearchFieldsActionTypes.success,
		fields,
	};
}

export function onFetchSearchFieldsFailed(error) {
	return {
		type: fetchSearchFieldsActionTypes.failure,
		error,
	};
}
