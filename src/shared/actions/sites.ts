import {
	getSitesActionTypes,
} from '../constants';

export function onGetSitesRequested() {
	return {
		type: getSitesActionTypes.processing,
	};
}

export function onGetSitesSucceeded(sites) {
	return {
		type: getSitesActionTypes.success,
		sites,
	};
}

export function onGetSitesFailed(error) {
	return {
		type: getSitesActionTypes.failure,
		error,
	};
}
