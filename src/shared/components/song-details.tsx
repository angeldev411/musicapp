import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Spinner from './spinner';
import FaithlifeComments from './faithlife-comments';
import TaggedReferences from './tagged-references';
import RatingsControl from './ratings-control';
import { ISiteApi, ISong, IUserInfo } from '../interfaces';
import { selectCurrentSong, selectUser } from '../selectors';
import { getValueForLanguage, serializeQueryParameters } from '../utils';
import { onUpdateUserMetadataRequested } from '../actions';

interface ISongDetailsProps {
	song: {
		isProcessing: boolean,
		details: ISong,
		error: string,
	},
	isSignedIn: boolean,
	updateUserMetadata: (songId, userData) => void,
}

interface IDetailsRowProps {
	title: string,
	isHidden: boolean,
	children: any,
	rowClassNames?: string,
	contentClassNames?: string
}

interface ITagsPanelProps {
	title: string,
	items: any[],
	urlMapper: (item) => string,
	labelMapper: (item) => string,
}

interface IShareFormProps {
	title: string,
	isHidden: boolean,
}

const DetailsRow = (props: IDetailsRowProps) => {
	const { title, isHidden, children, rowClassNames, contentClassNames } = props;
	if (isHidden) {
		return (<noscript />);
	}

	return (
		<div className={`row ${rowClassNames}`}>
			<span className="col-sm-2 detail-label">{title}</span>
			<div className={`col-sm-10 ${contentClassNames}`}>{children}</div>
		</div>
	);
};

const TagsPanel = (props: ITagsPanelProps) => {
	const { title, items, urlMapper, labelMapper } = props;
	if (items.length === 0) {
		return (<noscript />);
	}

	return (
		<div className="card">
			<header>{title}</header>
			<div className="tags">
				{
					items.map(item => (
						<span key={urlMapper(item)} className="label label-default">
							<Link to={`/?${urlMapper(item)}`}>
								{labelMapper(item)}
							</Link>
						</span>
					))
				}
			</div>
		</div>
	);
};

const ShareForm = (props: IShareFormProps) => {
	const { title, isHidden } = props;
	if (isHidden) {
		return (<noscript />);
	}
	const shareUrl = window.location.href;

	return (
		<div className="share-menu">
			<label htmlFor="share-url" className="share-label">{title}</label>
			<input type="text" id="share-url" className="share-url" value={shareUrl} readOnly />
			<div className="share-menu-mail-link">
				<a href={`mailto:?body=${shareUrl}`}>Email</a>
			</div>
		</div>
	);
};

const buildRatingAverage = (rating, ratingCount) => {
	if (!rating) {
		return '0 ratings';
	}
	const template = ratingCount === 1 ? '%count% rating' : '%count% ratings';
	const value = template.replace('%count%', ratingCount.toString());
	return `${rating.toFixed(1)} (${value})`;
};

class SongDetails extends React.Component<ISongDetailsProps> {
	state = {
		showSharePanel: false,
	}

	handleShareClick = () => {
		this.setState({
			showSharePanel: !this.state.showSharePanel,
		});
	}

	handleFavoriteClick = () => {
		const { userData, id } = this.props.song.details;
		this.props.updateUserMetadata(id, { ...userData, favorite: !userData.favorite });
	}

	render() {
		const { song: { isProcessing, details, error }, isSignedIn } = this.props;
		const { showSharePanel } = this.state;
		const favorite = !isProcessing && details.userData ? details.userData.favorite : false;
		const { rating, ratingCount } = details.appData;

		const viewTemplate = details.appData.views === 1 ? '%count% view' : '%count% views';
		const viewValue = viewTemplate.replace('%count%', details.appData.views.toString());

		const favoriteTemplate = details.appData.favoriteCount === 1 ? '%count% favorite' : '%count% favorites';
		const favoriteValue = favoriteTemplate.replace('%count%', details.appData.favoriteCount.toString());

		return (
			<div className="details-view">
				<div className="container">
					<header className="row song-title">
						<div className="col-md-12 card-container">
							<div className="card">
								{
									!isProcessing && <h1>{`${details.title}`}</h1>
								}
							</div>
						</div>
					</header>
					<div className="details-subheader row">
						<RatingsControl />
						<span>
							{buildRatingAverage(rating, ratingCount)}
						</span>
						&nbsp;&bull;&nbsp;
						<span>{favoriteValue}</span>
						&nbsp;&bull;&nbsp;
						<span>{viewValue}</span>
					</div>
					<div className="row desktop-view">
						<div className="col-md-8 card-container">
							<div className="card">
								<header>Details</header>
								<DetailsRow title="Alternate Titles" isHidden={details.alternateTitles.length === 0} rowClassNames="form-group">
									{
										details.alternateTitles.map((title, index) => (
											<div key={`${title}-${index}`}>
												<span>{title}</span>
											</div>
										))
									}
								</DetailsRow>
								<DetailsRow title="First Line" isHidden={!details.firstLine}>
									<span>
										{ details.firstLine }
									</span>
								</DetailsRow>
								<DetailsRow title="References" isHidden={details.references.length === 0} contentClassNames="ref-tagger-references">
									<TaggedReferences references={details.references} />
								</DetailsRow>
								<DetailsRow title="Themes" isHidden={details.themes.length === 0}>
									{
										details.themes.map(theme => theme.name).join('; ')
									}
								</DetailsRow>

								<DetailsRow title="CCLI" isHidden={details.ccliIds.length === 0}>
									{
										details.ccliIds.map((cclid, index) => (
											<span key={cclid}>
												<a href={`https://songselect.ccli.com/songs/${cclid}`} target="_blank" rel="noopener noreferrer">{`${cclid}`}</a>{(details.ccliIds.length - 1) > index ? '; ' : ''}
											</span>
										))
									}
								</DetailsRow>

								<DetailsRow title="Meter" isHidden={!details.meter}>
									<span>
										{ details.meter }
									</span>
								</DetailsRow>

								<DetailsRow title="Date" isHidden={!details.date.text}>
									<span>
										{ details.date.text }
									</span>
								</DetailsRow>
							</div>
							<div className="card">
								{
									details.linkGroups.map(group => {
										const category = group.displayName;
										return (
											<section key={`${category}`}>
												<header className="link-group-header">
													<span>{category}</span>
												</header>
												<ul>
													{
														group.links.map(item => (
															<li key={`${item.url.href}`} className="link-preview">
																<a href={item.url.href}>{`${item.title} `}</a>
																{
																	(category === 'Watch' || category === 'Listen') && <span className="link-hostname">{item.url.hostname}</span>
																}
															</li>
														))
													}
												</ul>
											</section>
										);
									})
								}
							</div>
							<FaithlifeComments songId={details.id} />
						</div>
						<div className="col-md-4 card-container">
							<div className="card">
								<div className="text-center">
									<button
										disabled={!isSignedIn}
										className={`icon-btn icon-btn-favorite ${favorite ? 'active' : ''}`}
										title={favorite ? 'Remove from favorites' : 'Add to favorites'}
										onClick={this.handleFavoriteClick}
									>
										<svg
											className="button-icon"
											version="1.1"
											id="Layer_1"
											xmlns="http://www.w3.org/2000/svg"
											xmlnsXlink="http://www.w3.org/1999/xlink"
											x="0px"
											y="0px"
											width="36px"
											height="36px"
											viewBox="0 0 79.979 72.003"
											enableBackground="new 0 0 79.979 72.003"
											xmlSpace="preserve"
										>
											<path fill="#DDDDDD" d="M58.088,4c4.405,0,8.856,1.748,12.321,5.233c5.471,5.51,7,13.83,4.221,20.67l-33.011,38.1h-3.26l-33.011-38.1c-2.779-6.84-1.25-15.16,4.221-20.67C13.035,5.748,17.486,4,21.891,4c2.395,0,4.777,0.517,6.979,1.553l11.12,9.771l11.12-9.771C53.312,4.517,55.693,4,58.088,4 M58.088,0c-3.034,0-5.955,0.65-8.681,1.934c-0.34,0.159-0.656,0.366-0.938,0.614l-8.48,7.451l-8.48-7.451c-0.282-0.248-0.598-0.455-0.938-0.614C27.846,0.65,24.925,0,21.891,0C16.228,0,10.845,2.277,6.733,6.413c-6.472,6.519-8.517,16.562-5.089,24.996c0.165,0.406,0.396,0.782,0.683,1.113l33.011,38.1c0.76,0.877,1.863,1.381,3.023,1.381h3.26c1.16,0,2.263-0.504,3.023-1.381l33.011-38.1c0.287-0.331,0.518-0.707,0.683-1.113c3.427-8.434,1.382-18.478-5.088-24.994C69.135,2.277,63.751,0,58.088,0L58.088,0z" />
										</svg>
										<div className="icon-btn-text">Favorite</div>
									</button>

									<button
										className="icon-btn icon-btn-share"
										onClick={this.handleShareClick}
									>
										<svg
											className="button-icon"
											version="1.1"
											id="Layer_1"
											xmlns="http://www.w3.org/2000/svg"
											xmlnsXlink="http://www.w3.org/1999/xlink"
											x="0px"
											y="0px"
											width="36px"
											height="36px"
											viewBox="0 0 86.838 75.601"
											enableBackground="new 0 0 86.838 75.601"
											xmlSpace="preserve"
										>
											<g>
												<path fill="#85A5E1" d="M85.518,20l-24-20l-2,0.09V14c-9.95,0.07-36.189,2.83-40,34c0,0,8.93-15.359,40-16v13.997c0.016-0.011,0.222-0.015,0.496-0.015c0.593,0,1.504,0.018,1.504,0.018l24-20C87.277,24.58,87.277,21.42,85.518,20z M82.837,23.027l-19.319,16.1V32c0-1.075-0.433-2.105-1.201-2.857C61.568,28.409,60.563,28,59.518,28c-0.027,0-0.055,0-0.082,0.001c-15.442,0.318-25.849,4.137-32.589,8.176C33.998,20.804,49.668,18.069,59.546,18c2.198-0.016,3.972-1.802,3.972-4V6.873l19.319,16.1C82.838,22.99,82.838,23.01,82.837,23.027z" />
												<path fill="#85A5E1" d="M70,49.061c-1.104,0-2,0.896-2,2v20.54H4v-48h17.391c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2v52c0,1.104,0.896,2,2,2h68c1.104,0,2-0.896,2-2v-22.54C72,49.956,71.104,49.061,70,49.061z" />
											</g>
										</svg>
										<div className="icon-btn-text">Share</div>
									</button>
								</div>
								<ShareForm title="Link" isHidden={!showSharePanel} />
							</div>
							<TagsPanel
								title="Authors"
								items={details.composers}
								urlMapper={item => serializeQueryParameters({ filter: `composers.name:"${item.name}"` })}
								labelMapper={item => item.name}
							/>
							<TagsPanel
								title="Translators"
								items={details.translators}
								urlMapper={item => serializeQueryParameters({ filter: `translators.name:"${item.name}"` })}
								labelMapper={item => item.name}
							/>
							<TagsPanel
								title="References"
								items={details.references}
								urlMapper={item => serializeQueryParameters({ filter: `references:"${item.text}"` })}
								labelMapper={item => item.text}
							/>
							<TagsPanel
								title="Themes"
								items={details.themes}
								labelMapper={item => item.name}
								urlMapper={item => serializeQueryParameters({ filter: `themes:"${item.name}"` })}
							/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	song: selectCurrentSong(state),
	isSignedIn: selectUser(state).details.id !== -1,
});

const mapDispatchToProps = {
	updateUserMetadata: (songId, userData) => onUpdateUserMetadataRequested(songId, userData),
};

export default connect(mapStateToProps, mapDispatchToProps)(SongDetails);
