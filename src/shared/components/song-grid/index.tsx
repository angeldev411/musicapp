import * as React from 'react';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { Link } from 'react-router-dom';

import Spinner from '../spinner';
import { ISiteApi, ISearchResult, ISong } from '../../interfaces';
import {
	onFetchMoreSearchResultsRequested,
	onFetchSongDetailsRequested,
	onFetchSongDetailsSucceeded,
} from '../../actions';
import { selectSongs, selectCurrentSong } from '../../selectors';
import { getValueForLanguage } from '../../utils';
import SongGridRow from './song-grid-row';

interface ISongGridProps {
	songs: {
		isProcessing: boolean,
		details: ISearchResult,
		error: string,
	},
	currentSong: {
		isProcessing: boolean,
		details: ISong,
		error: string,
	},
	loadMoreSearchResults: () => {},
	loadSongDetails: (slug: string) => void,
	clearSongDetails: () => void,
}

class SongGrid extends React.Component<ISongGridProps> {
	state = {
		selectedSongId: null,
	}

	componentWillMount() {
		this.props.clearSongDetails();
	}

	handleRowClick = (song: ISong) => {
		this.setState({ selectedSongId: song.id });
		this.props.loadSongDetails(song.slug);
	}

	render() {
		const { songs, loadMoreSearchResults } = this.props;
		const { selectedSongId } = this.state;

		return (
			<InfiniteScroll
				pageStart={0}
				loadMore={loadMoreSearchResults}
				initialLoad={false}
				hasMore={songs.details.hits.length < songs.details.total}
				useWindow={false}
				loader={null}
			>
				<table className="table-view">
					<thead>
						<tr className="table-header">
							<th className="title-header">Title</th>
							<th className="author-header">Author</th>
							<th className="themes-header">References</th>
							<th className="themes-header">Themes</th>
							<th className="rank-header">Rank</th>
						</tr>
					</thead>
					<tbody className="song-list">
						{
							songs.details.hits.map(({ hit: song }) => (
								<SongGridRow
									key={song.id}
									song={song}
									isSelected={selectedSongId === song.id}
									onClicked={this.handleRowClick}
								/>
							))
						}
					</tbody>
				</table>
				{
					songs.isProcessing && <Spinner scale=".4" top="40px" />
				}
			</InfiniteScroll>
		);
	}
}

const mapStateToProps = (state) => ({
	songs: selectSongs(state),
	currentSong: selectCurrentSong(state),
});

const mapDispatchToProps = {
	loadMoreSearchResults: onFetchMoreSearchResultsRequested,
	loadSongDetails: onFetchSongDetailsRequested,
	clearSongDetails: () => onFetchSongDetailsSucceeded(null),
};

export default connect(mapStateToProps, mapDispatchToProps)(SongGrid);
