import * as React from 'react';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { Link } from 'react-router-dom';

import { ISiteApi, ISearchResult, ISong } from '../../interfaces';

interface ISongGridRowProps {
	song: ISong,
	isSelected: boolean,
	onClicked: (song: ISong) => void,
}

class SongGridRow extends React.Component<ISongGridRowProps> {
	handleClick = () => {
		const {
			song,
			onClicked,
		} = this.props;
		onClicked(song);
	}

	render() {
		const { song, isSelected } = this.props;

		return (
			<tr className={`song table-data ${isSelected ? 'song-list-selected' : ''}`} onClick={this.handleClick}>
				<td className="title-data">
					<div className="title-link">
						<Link to={`/songs/${song.slug}`} >{ song.title }</Link>
					</div>
				</td>
				<td className="authors-data">{ song.composersDisplayString }</td>
				<td className="themes-data">{ song.referencesDisplayString }</td>
				<td className="themes-data">{ song.themesDisplayString }</td>
				<td className="rank-data">{ song.proclaimRank }</td>
			</tr>
		);
	}
}

export default SongGridRow;
