import * as React from 'react';
import { initializeScript } from '../utils';

interface IReference {
	reference: string;
	text: string;
}

interface ITaggedReferencesProps {
	references: IReference[];
}

/* eslint-disable react/no-danger */
class TaggedReferences extends React.PureComponent<ITaggedReferencesProps> {
	element: any;

	componentDidMount() {
		this.runRefTagger(this.props.references);
	}

	componentDidUpdate() {
		this.runRefTagger(this.props.references);
	}

	runRefTagger = (references) => {
		initializeScript(this, 'https://api.reftagger.com/v2/RefTagger.js');
	}

	initialize = () => {
		try {
			if (!window.refTagger.Initialized) {
				window.refTagger.init({
					bibleVersion: 'ESV',
					tagAutomatically: false,
					tagChapters: true,
				});
			}
			window.refTagger.tag(this.element);
		} catch (error) {
			console.log(error);
		}
	}

	initializeReference = (reference) => {
		this.element = reference;
	}

	render() {
		const referenceSpanStrings = this.props.references.map((reference) => (
			`<span>${reference.text}</span>`
		));

		return (
			<div
				ref={this.initializeReference}
				dangerouslySetInnerHTML={{ __html: referenceSpanStrings.join('') }}
			/>
		);
	}
}

export default TaggedReferences;
