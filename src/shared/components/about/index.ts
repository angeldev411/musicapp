import { connect } from 'react-redux';

import { selectSiteDetails } from '../../selectors';

import About from './presentation';

const mapStateToProps = (state) => ({
	sites: selectSiteDetails(state),
});

export default connect(mapStateToProps)(About);
