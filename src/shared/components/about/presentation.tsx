import * as React from 'react';

const About = ({
	sites,
}) => (
	<div className="about">
		<section>
			<header>About Faithlife Music</header>
			<p>Faithlife Music is a database of Christian music for the church, indexed by Scripture passage, Preaching Theme, and more.</p>
			<p>The site features metadata on hymns and worship songs as well as links to third-party sites where you will find lyrics, chord charts, lead sheets, sheet music, and more.</p>
			<p>Many of the songs indexed at Faithlife Music are copyrighted. For permissions, please contact the copyright holders.</p>
			<p>You can learn more about music copyright and licensing at <a href="https://ccli.com" target="_blank" rel="noopener noreferrer">CCLI</a>.</p>
			<p>Faithlife Music is a service of <a href="https://faithlife.com" target="_blank" rel="noopener noreferrer">Faithlife Corporation</a>, makers of <a href="https://logos.com" target="_blank" rel="noopener noreferrer">Logos BIble Software</a> and <a href="https://proclaimonline.com" target="_blank" rel="noopener noreferrer">Proclaim Church Presentation Software</a>.</p>
			<p>Join the conversation at in our <a href="https://faithlife.com/faithlife-music" target="_blank" rel="noopener noreferrer">Faithlife Group</a>!</p>
		</section>

		{ sites.length > 0 &&
			<section>
				<header>Approved sites</header>
				<ul>
					{ sites.map((site) => (
						<li key={site.url}>
							<a href={`https://${site.url}`} target="_blank" rel="noopener noreferrer">{site.url}</a>
						</li>
					))}
				</ul>
			</section>
		}
	</div>
);

export default About;
