import * as React from 'react';
import { Link, LocationDescriptor } from 'react-router-dom';
import { withRouter } from 'react-router';

import { IFilter } from '../../interfaces';

import {
	stringifyFilters,
	updateSearch,
} from '../../utils';

import Breadcrumb from './breadcrumb';

const removeFilter = (filters: IFilter[], index: number) =>
	filters.filter((_, idx) => idx !== index);

const removeFiltersAfter = (filters: IFilter[], index: number) => {
	if (index < 0) {
		return [];
	}

	return filters.slice(0, index + 1);
};

const getClearLocation = (index: number, selectedValues: IFilter[], location: LocationDescriptor) => {
	const filters = removeFilter(selectedValues, index);
	const search = updateSearch(location.search, {
		filter: stringifyFilters(filters),
	});

	return {
		...location,
		search,
	};
};

const getClearAfterLocation = (index: number, selectedValues: IFilter[], location: LocationDescriptor) => {
	const filters = removeFiltersAfter(selectedValues, index);
	const search = updateSearch(location.search, {
		filter: stringifyFilters(filters),
	});

	return {
		...location,
		search,
	};
};

const getClearSearchLocation = (location: LocationDescriptor) => '/';

const renderResultCount = (resultCount) => {
	const template = resultCount === 1 ? 'result' : 'results';
	return (
		<span className="count">({resultCount.toLocaleString('en-US')} {template})</span>
	);
};

const BreadcrumbBar = ({
	selectedValues,
	searchText,
	resultCount,
	location,
}) => (
	<ul className="filter-breadcrumb-bar">
		<li className="first hidden-xs">
			<Link to="/">Songs</Link>{' '}
		</li>
		{searchText && (
			<Breadcrumb
				title={`search: ${searchText}`}
				clearLocation={getClearSearchLocation(location)}
				clearAfterLocation={getClearAfterLocation(-1, selectedValues, location)}
			/>
		)}
		{selectedValues.map((filter, index) => (
			<Breadcrumb
				key={`${filter.facet}-${filter.term}`}
				title={filter.term}
				clearLocation={getClearLocation(index, selectedValues, location)}
				clearAfterLocation={getClearAfterLocation(index, selectedValues, location)}
			/>
		))}
		{' '}
		{typeof resultCount === 'number' && (
			<li className="filter-results hidden-xs">
				{ renderResultCount(resultCount) }
			</li>
		)}
	</ul>
);

export default withRouter(BreadcrumbBar);
