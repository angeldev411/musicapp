import * as React from 'react';
import { Link } from 'react-router-dom';

const Breadcrumb = ({
	title,
	clearLocation,
	clearAfterLocation,
}) => (
	<li className="filter-breadcrumb">
		{' '}
		<Link
			className="search-breadcrumb-x"
			to={clearLocation}
		>
			<div className="remove-query" />
		</Link>
		{' '}
		<Link to={clearAfterLocation}>
			{title}
		</Link>
	</li>
);

export default Breadcrumb;
