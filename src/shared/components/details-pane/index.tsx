import * as React from 'react';
import { connect } from 'react-redux';
import {
	selectCurrentSong,
} from '../../selectors';
import Spinner from '../spinner';
import RatingsControl from '../ratings-control';
import AlternateTitles from './alternate-titles';
import Composers from './composers';
import Themes from './themes';
import References from './references';
import LinkGroups from './link-groups';
import Section from './section';

const buildRatingText = (ratingCount) => {
	const template = ratingCount === 1 ? '%count% rating' : '%count% ratings';
	const value = template.replace('%count%', ratingCount.toString());
	return value;
};

const DetailsPane = ({
	song,
}) => {
	if (song.isProcessing) {
		return <Spinner scale=".4" top="40px" />;
	}

	if (song.error) {
		return (
			<p className="no-search-results">
				Failed to load the selected song details.
			</p>
		);
	}

	if (!song.details) {
		return (
			<div className="selection-details">
				<p className="no-search-results">
					Select a song to see its details.
				</p>
			</div>
		);
	}

	const {
		title,
		alternateTitles,
		firstLine,
		composers,
		themes,
		references,
		proclaimRank,
		date,
		meter,
		linkGroups,
		appData,
	} = song.details;

	return (
		<div className="selection-details">
			<article>
				<ul>
					<Section label="Title" value={title} />
					<AlternateTitles alternateTitles={alternateTitles} />
					<Section label="First Line" value={firstLine} />
					<Composers composers={composers} />
					<Themes themes={themes} />
					<References references={references} />
					<li>
						<span>Rating</span>
						<div className="ratings-control-container">
							<RatingsControl />
							<span>
								{buildRatingText(appData.ratingCount)}
							</span>
						</div>
					</li>
					<Section label="Rank" value={proclaimRank} />
					<Section label="Date" value={date.text} />
					<Section label="Meter" value={meter} />
					<LinkGroups linkGroups={linkGroups} />
				</ul>
			</article>
		</div>
	);
};

const mapStateToProps = (state) => ({
	song: selectCurrentSong(state),
});

export default connect(mapStateToProps)(DetailsPane);
