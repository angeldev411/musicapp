import * as React from 'react';
import { Link } from 'react-router-dom';
import queryString from 'query-string';

const renderTheme = ({ name }) => {
	if (!name) {
		throw new Error('Themes must have name.');
	}

	const search = queryString.stringify({
		q: `themes:"${name}"`,
	});

	return (
		<span key={name} className="label label-default">
			<Link to={{ pathname: '/', search }}>{name}</Link>
		</span>
	);
};

const Themes = ({ themes }) => (
	themes.length > 0 ? (
		<li>
			<span>Themes</span>
			<div className="tags">
				{ themes.map(renderTheme) }
			</div>
		</li>
	) : null
);

export default Themes;
