import * as React from 'react';

const Section = ({ label, value }) => (
	value ? (
		<li>
			<span>{label}</span>
			<p>{value}</p>
		</li>
	) : null
);

export default Section;
