import * as React from 'react';
import { Link } from 'react-router-dom';
import queryString from 'query-string';

const Reference = ({ text }) => {
	if (!text) {
		throw new Error('References must have `text`.');
	}

	const search = queryString.stringify({
		q: `references:<${text}>`,
	});

	return (
		<span className="label label-default">
			<Link to={{ pathname: '/', search }}>{text}</Link>
		</span>
	);
};

const References = ({ references }) => (
	references.length > 0 ? (
		<li>
			<span>References</span>
			<div className="tags">
				{ references.map(reference => <Reference key={reference.text} text={reference.text} />) }
			</div>
		</li>
	) : null
);

export default References;
