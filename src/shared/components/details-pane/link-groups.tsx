import * as React from 'react';

const LinkPreview = ({
	link,
	showHostName,
}) => (
	<li>
		{ (!link.preview || link.preview.kind !== 'media') &&
			<div className="link-preview">
				<a target="_blank" href={link.sourceUrl} rel="noopener noreferrer">{link.title || link.sourceUrl} {link.meta && link.meta.artist ? ` (${link.meta.artist})` : ''}</a>
				{ showHostName && <span className="link-hostname">{link.url.hostname}</span> }
			</div>
		}
		{ (link.preview && link.preview.kind === 'media') &&
			<div className="media">
				<div className="media-body media-middle mobile-view-sm">
					<h4 className="media-heading">
						<a href={link.sourceUrl}>
							{link.title}
						</a>
					</h4>
					{ showHostName &&
						<div className="media-content link-hostname">{link.url.hostname}</div>
					}
				</div>
				<div className="media-left">
					{ !link.preview.videoEmbedUrl &&
						<img src={link.preview.thumbnailUrl} alt={link.title} className="media-object" />
					}
					{ link.preview.videoEmbedUrl &&
						<iframe className="media-object" src={link.preview.videoEmbedUrl} frameBorder="0" allowFullScreen />
					}
				</div>
				<div className="media-body media-middle desktop-view-sm">
					<h4 className="media-heading">
						<a href={link.sourceUrl}>
							{link.title}
						</a>
					</h4>
					{ showHostName &&
						<div className="media-content link-hostname">{link.url.hostname}</div>
					}
				</div>
			</div>
		}
	</li>
);

const LinkGroups = ({ linkGroups }) => (
	linkGroups.length > 0 ? (
		<li>
			{ linkGroups.map(group => (
				<section key={group.displayName}>
					<span>{group.displayName}</span>
					<ul>
						{ group.links.map(link => <LinkPreview key={link.sourceUrl} link={link} showHostName={false} />) }
					</ul>
				</section>
			))}
		</li>
	) : null
);

export default LinkGroups;
