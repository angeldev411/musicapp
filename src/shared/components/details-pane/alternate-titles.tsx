import * as React from 'react';

const AlternateTitles = ({ alternateTitles }) => (
	alternateTitles.length > 0 ? (
		<li>
			<span>Alternate Titles</span>
			{ alternateTitles.map(alternateTitle => <p key={alternateTitle}>{alternateTitle}</p>) }
		</li>
	) : null
);

export default AlternateTitles;
