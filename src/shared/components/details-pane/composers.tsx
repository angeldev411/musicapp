import * as React from 'react';
import { Link } from 'react-router-dom';
import queryString from 'query-string';

const Author = ({ name }) => {
	if (!name) {
		throw new Error('Authors must have `name`.');
	}

	const search = queryString.stringify({
		q: `composers:"${name}"`,
	});

	return (
		<span className="label label-default">
			<Link to={{ pathname: '/', search }}>{name}</Link>
		</span>
	);
};

const Composers = ({ composers }) => (
	composers.length > 0 ? (
		<li>
			<span>Authors</span>
			<div className="tags">
				{ composers.map((composer, index) => <Author key={`${composer.name}-${index}`} name={composer.name} />) }
			</div>
		</li>
	) : null
);

export default Composers;
