import * as React from 'react';
import { withRouter, LocationDescriptor } from 'react-router';
import queryString from 'query-string';

import { sortOptions } from '../constants';
import { updateSearch } from '../utils';

import SearchBar from './search-bar';

interface ISortListProps {
	history: {
		push: (to: LocationDescriptor) => void,
	},
	location: LocationDescriptor,
}

interface ISortListState {
	sort: string,
}

class SortList extends React.Component<ISortListProps> {
	handleSortOptionChange = (ev) => {
		const { history, location } = this.props;
		const sort = ev.target.value;
		const sortQuery = sort === sortOptions[0].value ? undefined : sort;
		const newSearch = updateSearch(location.search, {
			sort: sortQuery,
		});

		history.push({
			...location,
			search: newSearch,
		});
	}

	render() {
		const { location } = this.props;
		const { sort } = queryString.parse(location.search);

		if (location.pathname !== '/') {
			return null;
		}

		return (
			<select value={sort} onChange={this.handleSortOptionChange}>
				{ sortOptions.map((option) => (
					<option key={option.value} value={option.value}>{option.label}</option>
				))}
			</select>
		);
	}
}

export default withRouter(SortList);
