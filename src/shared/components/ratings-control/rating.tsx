import * as React from 'react';

interface IRatingProps {
	rating: number,
	value: number,
	onRatingClicked: (value: number) => void,
}

class Rating extends React.Component<IRatingProps> {
	handleClick = () => {
		const {
			value,
			onRatingClicked,
		} = this.props;
		onRatingClicked(value);
	}
	render() {
		const {
			rating,
			value,
		} = this.props;

		return (
			<li className={`${rating ? 'has-rating' : ''} ${(rating && rating >= value) ? 'has-rating-highlight' : ''}`}>
				<button onClick={this.handleClick} />
			</li>
		);
	}
}

export default Rating;
