import * as React from 'react';
import { connect } from 'react-redux';

import { ISong } from '../../interfaces';
import {
	onUpdateUserMetadataRequested,
} from '../../actions';
import {
	selectCurrentSong,
	selectUser,
} from '../../selectors';

import Rating from './rating';

const max = 5;
const values: number[] = [];

for (let i = 1; i <= max; i++) {
	values.push(i);
}

interface IRatingControlProps {
	isSignedIn: boolean,
	song: ISong,
	requestUpdateUserMetadata: (songId: string, userData: ISong) => void,
}

class RatingControl extends React.Component<IRatingControlProps> {
	handleSubmitRating = (value: number) => {
		const { song } = this.props;
		const userData = {
			...song.userData,
			rating: value,
		};
		this.props.requestUpdateUserMetadata(song.id, userData);
	}

	render() {
		const {
			isSignedIn,
			song,
		} = this.props;

		const { rating } = song.userData;
		return (
			<div className="ratings-control">
				<div className="star-ratings">
					<ul className={isSignedIn ? '' : 'readonly'}>
						{ values.map(value => (
							<Rating
								key={value}
								rating={rating}
								value={value}
								onRatingClicked={this.handleSubmitRating}
							/>
						))}
					</ul>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	isSignedIn: selectUser(state).details.id !== -1,
	song: selectCurrentSong(state).details,
});

const mapDispatchToProps = {
	requestUpdateUserMetadata: onUpdateUserMetadataRequested,
};

export default connect(mapStateToProps, mapDispatchToProps)(RatingControl);
