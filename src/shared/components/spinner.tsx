import * as React from 'react';
import RSpinner from 'react-spin';


interface ISpinnerProps {
	scale?: string,
	left?: string,
	top?: string,
}
const spinConfig = {
	lines: 13,
	length: 25,
	width: 6,
	radius: 30,
	scale: 1,
	corners: 1,
	rotate: 9,
	direction: 1,
	color: '#000',
	speed: 1.4,
	trail: 68,
	shadow: false,
	className: 'spinner',
	zIndex: 1,
	top: 'auto',
	left: 'auto',
	position: 'relative',
};

// eslint-disable-next-line react/prefer-stateless-function
export default class Spinner extends React.Component<ISpinnerProps> {
	render() {
		const { scale, left, top } = this.props;
		return (
			<RSpinner
				config={{
					...spinConfig,
					scale: parseFloat(scale as string),
					left,
					top,
				}}
			/>
		);
	}

}
