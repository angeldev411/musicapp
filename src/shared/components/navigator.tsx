import * as React from 'react';

import { withRouter, LocationDescriptor } from 'react-router';

import { navigate } from '../routes';

interface INavigatorProps {
	match: object,
	location: LocationDescriptor,
	history: any,
}

class Navigator extends React.Component<INavigatorProps> {
	static contextTypes = {
		store: React.PropTypes.object,
	}

	unlisten = () => {}

	componentDidMount() {
		this.unlisten();
		this.unlisten = this.props.history.listen((location, action) => {
			navigate(location.pathname, location.search, this.context.store);
		});
	}

	componentWillUnmount() {
		this.unlisten();
	}
	// eslint-disable-next-line class-methods-use-this
	render() {
		return <noscript />;
	}
}

export default withRouter(Navigator);
