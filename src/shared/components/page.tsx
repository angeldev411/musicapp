import * as React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { BrandBar } from '@faithlife/brand-bar';
import { Helmet } from 'react-helmet';

import * as localizedResources from '@faithlife/brand-bar/dist/locales/en-US/resources.json';
import '@faithlife/brand-bar/dist/brand-bar.css';

import { IUserInfo, ISiteApi } from '../interfaces';
import {
	onSignInRequested,
	onSignOutRequested,
	onRegisterRequested,
	onForgotPasswordRequested,
} from '../actions';
import { selectUser } from '../selectors';

import Header from './header';
import Navigator from './navigator';
import NotFound from './not-found';
import Home from './home';
import SongDetails from './song-details';
import About from './about';
import Help from './help';

declare global {
	interface Window {
		refTagger: any;
	}
}

const mapStateToBrandBarProps = state => {
	const {
		isProcessing,
		details,
		error,
	} = selectUser(state);

	const isAuthenticated = !isProcessing && details.id !== -1;

	return {
		isAuthenticated,
		isAuthProcessing: isProcessing,
		userName: isAuthenticated ? details.name!.value : null,
		avatarUrl: isAuthenticated ? details.avatarUrls!.value.avatarUrl : null,
		authErrorMessage: error,
	};
};

const mapDispatchToBrandBarProps = {
	onSignInRequested,
	onSignOutRequested,
	onRegisterRequested,
	onForgotPasswordRequested,
};

const ConnectedBrandBar = connect(mapStateToBrandBarProps, mapDispatchToBrandBarProps)(BrandBar);

export default () => (
	<div>
		<Helmet>
			<meta property="og:url" content="https://music.faithlife.com" />
			<meta property="og:title" content="Faithlife Music" />
			<meta property="og:description" content="Faithlife Music helps you find Christian music by scripture or theme and links you to videos, chord charts, sheet music and more." />
		</Helmet>
		<ConnectedBrandBar usesAuthentication localizedResources={localizedResources} />
		<Header />
		<Navigator />
		<div className="router-view">
			<Switch>
				<Route exact path="/" component={Home} />
				<Route exact path="/songs/:slug" component={SongDetails} />
				<Route exact path="/about" component={About} />
				<Route exact path="/help" component={Help} />
				<Route component={NotFound} />
			</Switch>
		</div>
	</div>
	);
