import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, LocationDescriptor } from 'react-router';
import queryString from 'query-string';

import SearchBar from './presentation';

import { selectFacetsTotal } from '../../selectors';
import {
	parseFilterParameter,
	parseSearchParameter,
	updateSearch,
} from '../../utils';

interface SearchBarContainerProps {
	facetsTotal: number,
	location: LocationDescriptor,
	history: {
		push: (to: LocationDescriptor) => void,
	},
}

class SearchBarContainer extends React.Component<SearchBarContainerProps> {
	getSearchText = () => parseSearchParameter(this.props.location.search) || '';

	updateUrl = (searchText) => {
		const { location, history } = this.props;
		const { search } = location;

		const newSearch = updateSearch(search, {
			q: searchText,
		});

		history.push({
			...location,
			search: newSearch,
		});
	}

	handleKeyDown = (ev) => {
		if (ev.key === 'Escape') {
			this.handleSearchClear();
		}
	}

	handleSearchClear = () => {
		this.updateUrl('');
	}

	handleSearchQueryChange = (ev) => {
		this.updateUrl(ev.target.value);
	}

	handleSubmit = (ev) => {
		ev.preventDefault();

		const { history, location } = this.props;
		const searchText = this.getSearchText();
		const newSearch = updateSearch(location.search, {
			filter: null,
			q: searchText,
		});

		history.push({
			pathname: '/',
			search: newSearch,
		});
	}

	render() {
		const searchText = this.getSearchText();

		return (
			<SearchBar
				searchText={searchText}
				facetsTotal={this.props.facetsTotal}
				onSearchClear={this.handleSearchClear}
				onSearchQueryChange={this.handleSearchQueryChange}
				onKeyDown={this.handleKeyDown}
				onSubmit={this.handleSubmit}
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	facetsTotal: selectFacetsTotal(state),
});

export default withRouter(connect(mapStateToProps)(SearchBarContainer));
