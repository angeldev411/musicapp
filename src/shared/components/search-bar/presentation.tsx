import * as React from 'react';

const SearchBar = ({
	searchText,
	facetsTotal,
	onSearchClear,
	onSearchQueryChange,
	onKeyDown,
	onSubmit,
}) =>	(
	<form className="search-form" onSubmit={onSubmit}>
		<div className="search-bar">
			<input
				className="form-control"
				tabIndex={0}
				placeholder="Find&hellip;"
				spellCheck={false}
				value={searchText}
				onChange={onSearchQueryChange}
				onKeyDown={onKeyDown}
			/>
			<button className="hidden" type="submit" />
			<span className="visible-xs-inline-block filter-results">
				{ facetsTotal }
			</span>
		</div>
		<a
			className="hidden-xs btn-clear-search"
			onClick={onSearchClear}
		>
			<div className={searchText ? 'has-text' : ''} />
		</a>
	</form>
);

export default SearchBar;
