import * as React from 'react';

import FacetGroup from './facet-group';
import { IFacetGroupModel } from './models';
import { facetNames } from './constants';

const FacetList = ({
	facets,
	selectedFilters,
	moreLinkThreshold,
}) => (
	<section className="list-sidebar-section">
		<div className="facet-list">
			{ facets
				.filter((facet) => facet.name !== 'curated')
				.map((facet) => (
					<FacetGroup
						key={facet.field}
						facet={facet}
						selectedFilters={selectedFilters}
						moreLinkThreshold={moreLinkThreshold}
					/>
				))
			}
		</div>
	</section>
);

export default FacetList;
