import * as React from 'react';

import { IFilter } from '../../interfaces';
import { containsFilter } from '../../utils';

import FacetValue from './facet-value';
import { IFacetGroupModel } from './models';
import { facetNames } from './constants';

interface IFacetGroupProps {
	facet: IFacetGroupModel,
	selectedFilters: IFilter[],
	moreLinkThreshold: number,
}

interface IFacetGroupState {
	isExpanded: boolean,
}

const createFacetTerm = (facet, term) => ({
	facet: facet.name,
	term: facet.name === 'curated' ? convertBooleanValues(term.value) : term.value,
});

const convertBooleanValues = (termValue) => {
	const value = termValue.toLowerCase();
	let coercedValue;

	switch (value) {
	case 't':
		coercedValue = 'true';
		break;
	case 'f':
		coercedValue = 'false';
		break;
	default:
		coercedValue = termValue;
	}

	return coercedValue;
};

export default class FacetGroup extends React.Component<IFacetGroupProps, IFacetGroupState> {
	state = {
		isExpanded: false,
	}

	handleExpansionToggle = () => {
		this.setState({
			isExpanded: !this.state.isExpanded,
		});
	}

	render() {
		const { facet, moreLinkThreshold } = this.props;
		const { name, field, terms } = facet;
		const { isExpanded } = this.state;

		const shownTerms = terms.filter(term => !containsFilter(this.props.selectedFilters, createFacetTerm(facet, term)));
		const visibleTerms = isExpanded ? shownTerms : shownTerms.slice(0, moreLinkThreshold);

		if (visibleTerms.length === 0) {
			return null;
		}

		return (
			<section>
				<header className="facet-name">{facetNames[name] || name}</header>
				<ul className="facet-items">
					{ visibleTerms.map((term) => (
						<FacetValue
							key={term.value}
							field={field}
							value={term.value}
							count={term.count}
						/>))
					}
				</ul>
				{ shownTerms.length > moreLinkThreshold &&
					<button className="btn-link-inline facet-list-more" onClick={this.handleExpansionToggle}>
						<span>
							{ isExpanded ? '« Less' : 'More »' }
						</span>
					</button>
				}
			</section>
		);
	}
}
