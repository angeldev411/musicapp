export interface IFacetGroupModel {
	name: string,
	field: string,
	terms: IFacetValueModel[],
}

export interface IFacetValueModel {
	value: string,
	count: number,
}
