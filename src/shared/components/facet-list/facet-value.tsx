import * as React from 'react';
import { withRouter, LocationDescriptor } from 'react-router';
import { Link } from 'react-router-dom';

import { facetNames } from './constants';

import { updateSearch, parseFilterParameter, stringifyFilters, containsFilter } from '../../utils';
import { IFilter } from '../../interfaces';

interface IFacetValueProps {
	field: string,
	value: string,
	count: number,
	location: LocationDescriptor,
}

const addFilter = (filters: IFilter[], filter: IFilter) => (
	containsFilter(filters, filter) ? filters : filters.concat([ filter ])
);

export class FacetValue extends React.Component<IFacetValueProps> {
	getNewQuery = () => {
		const {
			field,
			value,
			location,
		} = this.props;

		let filters = parseFilterParameter(location.search);
		filters = addFilter(filters, {
			facet: field,
			term: value,
		});

		return updateSearch(location.search, {
			filter: stringifyFilters(filters),
		});
	}

	render() {
		const { value, count, location } = this.props;

		return (
			<li>
				<Link
					className="btn-link-inline"
					to={{
						...location,
						search: this.getNewQuery(),
					}}
				>
					{ value }
				</Link>
				{' '}
				<span className="facet-list-term-count">({count})</span>
			</li>
		);
	}
}

export default withRouter(FacetValue);
