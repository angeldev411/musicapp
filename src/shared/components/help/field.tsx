import * as React from 'react';

import Subfield from './subfield';

const Field = ({
	field,
}) => (
	<span>
		<dt>
			{ field.name }
			{
				field.aliases && field.aliases.map(alias => (
					<span key={alias}>, {alias}</span>
				))
			}
			{ field.isDefault && <span className="note" title="This field is included by default when no field name is specified">&ensp;&#9733;</span> }
		</dt>
		<dd>{field.description}</dd>
		{ field.children &&
			<dd>
				<dl>
					{
						field.children.map(child => (
							<Subfield key={child.name} field={field} child={child} />
						))
					}
				</dl>
			</dd>
		}
	</span>
);

export default Field;
