import * as React from 'react';
import { connect } from 'react-redux';

import { selectSearchFields } from '../../selectors';

import Field from './field';

const Help = ({
	fields,
}) => (
	<div className="search-help container-fluid">
		<div className="row">
			<div className="col-md-4 col-md-push-8">
				<div className="card">
					<h2>I want to find</h2>
					<dl>
						<dt>&hellip; the song "Amazing Grace"</dt>
						<dd><code>Amazing Grace</code></dd>
						<dd><code>title:"Amazing Grace"</code> &mdash; only songs with a title that matches &ldquo;Amazing Grace&rdquo;</dd>
						<dt>&hellip; songs by Charles Wesley</dt>
						<dd><code>Charles Wesley</code></dd>
						<dd><code>composers:"Charles Wesley"</code></dd>
						<dd><code>title:"Jesus, Lover of My Soul" composers:"Charles Wesley"</code> &mdash; songs which have &ldquo;Jesus, Lover of My Soul&rdquo; in the title and are composed by Charles Wesley</dd>
					</dl>
				</div>
			</div>

			<div className="col-md-8 col-md-pull-4">
				<div className="card">
					<h2>Search fields</h2>
					<dl>
						{
							fields && fields.map(field => (
								<Field key={field.name} field={field} />
							))
						}
					</dl>
				</div>
			</div>
		</div>
	</div>
);

const mapStateToProps = (state) => ({
	fields: selectSearchFields(state).details,
});

export default connect(mapStateToProps)(Help);
