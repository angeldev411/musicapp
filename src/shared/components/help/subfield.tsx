import * as React from 'react';

const Subfield = ({
	field,
	child,
}) => (
	<span>
		<dt>
			{field.name}.{child.name}
			{child.aliases && child.aliases.map(alias => (
				<span key={alias}>, {field.name}.{alias}</span>
			))}
		</dt>
		<dd>{child.description}</dd>
	</span>
);

export default Subfield;
