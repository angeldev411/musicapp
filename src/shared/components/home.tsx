import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, LocationDescriptor } from 'react-router';
import { Link } from 'react-router-dom';
import { FaithlifeAd } from '@faithlife/faithlife-ad';

import FacetList from './facet-list';
import BreadcrumbBar from './breadcrumb-bar';
import SongGrid from './song-grid';
import { IFacetGroupModel } from './facet-list/models';
import DetailsPane from './details-pane';

import {
	IFilter,
} from '../interfaces';
import {
	selectFacets,
	selectFacetsTotal,
	selectDetailsPaneVisibility,
} from '../selectors';
import {
	parseFilterParameter,
	parseSearchParameter,
	serializeQueryParameters,
} from '../utils';

interface IHomeProps {
	facets: IFacetGroupModel[],
	facetsTotal: number,
	location: LocationDescriptor,
	isDetailsPaneVisible: boolean,
}

const Home = ({
	facets,
	facetsTotal,
	location,
	isDetailsPaneVisible,
}) => {
	const selectedValues = parseFilterParameter(location.search);
	const searchText = parseSearchParameter(location.search);

	return (
		<div className={`list-container container-fluid ${isDetailsPaneVisible ? 'list-container-show-sidebar' : ''}`}>
			<div className="sidebar-left main-view">
				<FacetList
					facets={facets}
					selectedFilters={selectedValues}
					moreLinkThreshold={6}
				/>
				<nav>
					<ul>
						<li>
							<Link to="/about">About Faithlife Music</Link>
						</li>
						<li>
							<Link to="/help">Search Help</Link>
						</li>
					</ul>
				</nav>
			</div>
			{
				isDetailsPaneVisible && (
					<div className="sidebar-right main-view">
						<DetailsPane />
					</div>
				)
			}
			<div className="main-view">
				<div className="cms-banner">
					<ul>
						<li>
							<Link to={`/?${serializeQueryParameters({ filter: 'lists.name:"Top 100"', sort: 'lists.rank:asc' })}`}>
								<img src="/images/top-100.jpg" alt="Top 100 songs" />
							</Link>
						</li>
						<li>
							<Link to={`/?${serializeQueryParameters({ filter: 'lists.name:"Chart climbers"', sort: 'lists.rank:asc' })}`}>
								<img src="/images/chart-climbers.jpg" alt="Chart climbers" />
							</Link>
						</li>
						<li>
							<FaithlifeAd zoneId={385} />
						</li>
					</ul>
				</div>
				<div className="list-top-bar hidden-xs">
					<BreadcrumbBar
						selectedValues={selectedValues}
						searchText={searchText}
						resultCount={facetsTotal}
					/>
				</div>
				<SongGrid />
			</div>
		</div>
	);
};

const mapStateToProps = (state) => ({
	facets: selectFacets(state),
	facetsTotal: selectFacetsTotal(state),
	isDetailsPaneVisible: selectDetailsPaneVisibility(state),
});

export default connect(mapStateToProps)(Home);
