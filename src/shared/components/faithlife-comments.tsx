import * as React from 'react';
import { initializeScript } from '../utils';

interface IFaithlifeCommentsProps {
	songId: string
}

declare global {
	interface Window {
		faithlife: any
	}
}

const getTargetUri = (songId) => {
	const environments = {
		local: 'local',
		internal: 'internal',
		production: 'production',
	};
	const host = window.location.hostname;
	const index = host.indexOf('.');
	const subdomains = {
		local: environments.local,
		internal: environments.internal,
	};
	const environment = (index === -1) ? environments.local : subdomains[host.substr(0, index)] || environments.production;
	const hostname = `${(environment === 'local' || environment === 'internal' ? 'internal.' : '')}musicapi.faithlife.com`;
	return `https://${hostname}/v1/songs/${songId}`;
};


export default class FaithlifeComments extends React.Component<IFaithlifeCommentsProps> {
	selfElement = null as any;

	initialize() {
		if (!this.selfElement) {
			return;
		}

		const targetUri = getTargetUri(this.props.songId);
		const faithlife = window.faithlife;
		try {
			if (faithlife && faithlife.plugins) {
				const commentElement = faithlife.plugins.createCommentsPlugin({
					width: '100%',
					height: 0,
					targetUri,
				});
				this.selfElement.appendChild(commentElement);
			}
		} catch (error) {
			console.log(error);
		}
	}

	componentDidMount() {
		initializeScript(this, 'https://faithlife.com/scripts/api/faithlifePlugins.js');
	}

	render() {
		return (
			<div
				className="card faithlife-reviews"
				ref={(e) => { this.selfElement = e; }}
			/>
		);
	}
}
