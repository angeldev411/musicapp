import * as React from 'react';
import { Link } from 'react-router-dom';

import SearchBar from './search-bar';
import SortList from './sort-list';
import DetailsButton from './details-button';

const Header = () => (
	<div className="nav-bar" >
		<nav className="main-toolbar">
			<div className="primary-controls">
				<Link className="logo home-icon" to="/">
					<img src="/images/logo.png" alt="logo" />
				</Link>
				<SearchBar />
			</div>
			<div className="subheader-container hidden-xs">
				<div className="subheader">
					<div className="list-controls">
						<SortList />
						<DetailsButton />
					</div>
				</div>
			</div>
		</nav>
	</div>
);

export default Header;
