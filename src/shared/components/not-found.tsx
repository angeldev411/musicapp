import * as React from 'react';
import { Route } from 'react-router-dom';

const Status = ({ code, children }) => (
	<Route
		render={({ staticContext }) => {
			if (staticContext) {
				// eslint-disable-next-line no-param-reassign
				staticContext.status = code;
			}
			return children;
		}}
	/>
);

const NotFound = () => (
	<Status code={404}>
		<div />
	</Status>
);

export default NotFound;
