import * as React from 'react';
import { connect } from 'react-redux';

import { onUpdateDetailsPaneVisibilityRequested } from '../actions';
import { selectDetailsPaneVisibility } from '../selectors';

interface IDetailsButtonProps {
	isDetailsPaneVisible: boolean,
	updateDetailsPaneVisibility: (visible: boolean) => void,
}

class DetailsButton extends React.Component<IDetailsButtonProps> {
	handleClick = () => {
		this.props.updateDetailsPaneVisibility(!this.props.isDetailsPaneVisible);
	}

	render() {
		const { isDetailsPaneVisible } = this.props;

		return (
			<button
				className={`btn ${isDetailsPaneVisible ? 'active' : ''}`}
				onClick={this.handleClick}
			>
				<div className="list-controls-svg-container">
					<svg className="button-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 28 28" enableBackground="new 0 0 28 28">
						<path fill="#096FB6" d="M18,28H0V0h18V28z M28,0h-8v28h8V0z" />
					</svg>
				</div>
			</button>
		);
	}
}

const mapStateToProps = (state) => ({
	isDetailsPaneVisible: selectDetailsPaneVisibility(state),
});

const mapDispatchToProps = {
	updateDetailsPaneVisibility: onUpdateDetailsPaneVisibilityRequested,
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailsButton);
