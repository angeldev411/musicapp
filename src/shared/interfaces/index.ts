export interface IUserInfo {
	id: number,
	avatarUrls?: {
		value: {
			avatarUrl: string,
			avatarUrlLarge: string,
		},
	},
	name?: {
		value: string,
	},
}

export interface ISiteApi {
	// Accounts api
	getCurrentUserAsync: () => Promise<IUserInfo>,
	signInAsync: ({ email, password }) => Promise<IUserInfo>,
	registerAsync: ({ name, email, password }) => Promise<{}>,
	forgotPasswordAsync: ({ email }) => Promise<{}>,
	signOutAsync: () => Promise<{}>,

	// Music api
	getSitesAsync: () => Promise<{}>,
	searchSongAsync: (query, limit, sort) => Promise<{}>,
	trackViewsAsync: (songId) => Promise<{}>,
	updateUserDataAsync: (songId, userData) => Promise<{}>,
	getSongDetailsAsync: (slug, revisionId?) => Promise<{}>,
	getLinkPreviewsAsync: (song) => Promise<{}>,
	getSongRevisionsAsync: (id, afterId, limit) => Promise<{}>,
	createUserLinkAsync: (songId, linkData) => Promise<{}>,
	deleteUserLinkAsync: (songId, url) => Promise<{}>,
	getUserLinksAsync: (songId) => Promise<{}>,
	editSongFieldAsync: (songId, fieldName, fieldValue, revisionId, arrayOptions) => Promise<{}>,
	createSongAsync: (song) => Promise<{}>,

	//Search api
	getSearchFieldsAsync: () => Promise<{}>,
	doSearchAsync: (query, limit, sort) => Promise<{}>,
	getMoreResultsAsync: (query, offset, limit, sort) => Promise<{}>,

	//Link-preview api
	getPreviewAsync: (url) => Promise<{}>,

	//Cookie api
	setCookie: (cookieName, cookieValue, expiryDays) => void,
}

export interface IApplicationState {
	user?: IUserInfo,
}

export interface ILocalizedString {
	string: string,
	language: string,
}

export interface ITerm {
	count: number,
	value: string,
}

export interface IName {
	name: string,
}
export interface IFacet {
	field: string,
	name: string,
	terms: ITerm[],
}

export interface ITheme {
	id: string,
	name: string,
}

export interface IReference {
	reference: string,
	displayNames: ILocalizedString[],
}
export interface ICategory {
	displayName: string,
	sortPriority: string,
}

export interface IUrl {
	protocol: string,
	hostname: string,
	pathname: string,
	search: string,
	source: string,
}
export interface ILink {
	sourceUrl?: string,
	url: IUrl,
	kind?: string,
	category?: ICategory,
	title: string,
	preview?: any,
	meta?: any
}

export interface IHit {
	hit: ISong,
}

export interface ISearchResult {
	facets: IFacet[],
	hits: IHit[],
	total: number,
}

export interface ISong {
	title: string,
	id: string,
	slug: string,
	alternateTitles: string[],
	themes: ITheme[],
	themesDisplayString: string,
	translators: any[],
	composers: any[],
	composersDisplayString: string,
	date: any,
	firstLine: Array<string>,
	references: Array<{
		reference: string,
		text: string,
	}>,
	referencesDisplayString: string,
	appData: {
		favoriteCount: number,
		lastViewed: string
		ratingCount: number,
		ratingTotal: number,
		rating: number,
		views: number,
	},
	lists: string[],
	language: string,
	ccliIds: number[],
	userData: any,
	links: ILink[],
	linkGroups: any[],
	meter: string,
	curated: boolean,
	revision: any,
	deleted: boolean,
	proclaimRank: string;
}

export interface IFilter {
	facet: string;
	term: string;
}

export const LinkKinds = {
	resource: 'resource',
	web: 'web',
	user: 'user',
};
