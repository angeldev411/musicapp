import { createStore, applyMiddleware, Store } from 'redux';
import createSagaMiddleware, { END, Task } from 'redux-saga';

import rootReducer from '../reducers';

export interface ISagaStore extends Store<object> {
	close: () => void,
	runSaga: (...params: any[]) => Task,
}

const configureStore = (initialState: object = {}) => {
	const sagaMiddleware = createSagaMiddleware();
	const store = createStore(
		rootReducer,
		initialState,
		applyMiddleware(sagaMiddleware),
	) as ISagaStore;

	store.runSaga = sagaMiddleware.run;
	store.close = () => store.dispatch(END);

	return store;
};

export default configureStore;
