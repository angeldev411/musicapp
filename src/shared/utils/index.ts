import URL from 'url-parse';
import queryString from 'query-string';

import { ISong, ILink, LinkKinds, IFilter } from '../interfaces';

export * from './script-async-loader';

export function createAsyncActionTypes(
	processing: string,
	success = `${processing}_SUCCESS`,
	failure = `${processing}_FAILURE`,
	canceled = `${processing}_CANCELED`,
) {
	return {
		processing,
		success,
		failure,
		canceled,
	};
}

export const serializeQueryParameters = (queryParameters: object, prefix: string = '') => Object.entries(queryParameters).map(([ key, value ]) => {
	const parameterName = prefix ? `${prefix}[${key}]` : key;
	return value && typeof value === 'object' ? serializeQueryParameters(value, parameterName) : `${encodeURIComponent(parameterName)}=${encodeURIComponent(value)}`;
}).join('&');

export const getValueForLanguage = (collection, language = 'en') => {
	if (!collection) {
		return '';
	}

	const pair = collection.find(value => value.language === language);
	return pair ? pair.string : '';
};

export const getValuesForLanguage = (collection, language = 'en') => {
	if (!collection) {
		return [];
	}

	return collection.filter(value => value.language === language).map(item => item.string);
};

export const getDisplayStringForArray = (collection, propertyName, limit = 0) => {
	if (!collection) {
		return '';
	}
	let items = collection.map(pair => pair[propertyName]);
	if (limit) {
		items = items.slice(0, limit);
	}

	return items.join(', ') + (collection.length > limit ? '...' : '');
};

const getCcliIds = (externalData) => {
	if (!externalData || externalData.length === 0) {
		return [];
	}

	return externalData.filter(item => item.kind === 'CCLI')
		.map(item => item.data.songNumber);
};

const parseUrl = (url) => {
	let newUrl = url;
	if (!/^https?:\/\//.test(url)) {
		newUrl = `http://${url}`;
	}

	return URL(newUrl);
};
const getCategory = (hostname) => {
	const host = hostname.toLowerCase().replace('www.', '');
	const video = { displayName: 'Watch', sortPriority: 1 };
	const audio = { displayName: 'Listen', sortPriority: 2 };
	const thirdParty = { displayName: host, sortPriority: 3 };

	const linkCategoryMap = {
		'youtube.com': video,
		'youtu.be': video,
		'vimeo.com': video,
		'amazon.com': audio,
		'itunes.apple.com': audio,
		'spotify.com': audio,
		'open.spotify.com': audio,
	};

	return linkCategoryMap[host] || thirdParty;
};

const getRanking = (kind, externalData) => {
	if (!externalData || externalData.length === 0) {
		return undefined;
	}

	const ranks = externalData.filter(item => item.kind === 'Proclaim')
		.map(item => item.data.rank);

	return ranks.length ? ranks[0] : undefined;
};

export const mapToSong = (songDto) => {
	const themes = (songDto.preachingThemes || []).map(theme => ({ id: theme.id, name: getValueForLanguage(theme.displayNames) }));
	const references = (songDto.references || []).map(ref => ({ reference: ref.reference, text: getValueForLanguage(ref.displayNames) }));
	const appData = songDto.appData;
	if (!appData.views) {
		appData.views = 1;
	}
	const links = (songDto.resourceSources || []).map(item => {
		const sourceUrl = `https://biblia.com/books/${item.textRange.resourceId}/article/${item.textRange.articleId}`;
		const parsedUrl = parseUrl(sourceUrl);
		return {
			sourceUrl,
			url: parsedUrl,
			title: item.title,
			category: getCategory(parsedUrl.hostname),
			kind: LinkKinds.resource,
		};
	});
	links.push(...(songDto.webSources || []).map(item => {
		const sourceUrl = item.url;
		const parsedUrl = parseUrl(sourceUrl);
		return {
			sourceUrl,
			url: parsedUrl,
			title: item.title,
			category: getCategory(parsedUrl.hostname),
			kind: LinkKinds.web,
			meta: { artist: item.artist, album: item.album },
		};
	}
	));

	const categories = {} as { [key: string]: { displayName: string, sortPriority: number, links: object[] } };
	for (const { category: linkCategory, ...link } of links) {
		let category = categories[linkCategory.displayName];
		if (!category) {
			category = { displayName: linkCategory.displayName, sortPriority: linkCategory.sortPriority, links: [] };
			categories[linkCategory.displayName] = category;
		}

		category.links.push(link);
	}

	const linkGroups = Object.values(categories)
		.sort((a, b) => a.sortPriority - b.sortPriority);

	return {
		title: getValueForLanguage(songDto.title),
		id: songDto.id,
		slug: songDto.slug,
		alternateTitles: getValuesForLanguage(songDto.alternateTitles),
		themes,
		themesDisplayString: getDisplayStringForArray(themes, 'name'),
		translators: songDto.translators || [],
		composers: songDto.composers || [],
		composersDisplayString: getDisplayStringForArray(songDto.composers, 'name'),
		date: songDto.date ? { reference: songDto.date.reference, text: getValueForLanguage(songDto.date.displayNames) } : { reference: undefined, text: '' },
		firstLine: getValueForLanguage((songDto.firstLine || [])),
		references,
		referencesDisplayString: getDisplayStringForArray(references, 'text'),
		appData: {
			...appData,
			rating: appData.ratingCount > 0 ? appData.ratingTotal / appData.ratingCount : 0,
		},
		lists: songDto.lists || [],
		language: songDto.language,
		ccliIds: getCcliIds(songDto.externalData),
		userData: songDto.userData,
		links,
		linkGroups,
		meter: songDto.meter,
		curated: songDto.curated,
		revision: songDto.revision,
		deleted: songDto.deleted,
		proclaimRank: getRanking('Proclaim', songDto.externalData),
	};
};

export const parseFilterParameter = (search: string) => {
	const query = queryString.parse(search);
	if (!query.filter) {
		return [];
	}

	return query.filter
		.split(',')
		.map(pair => {
			const colonPosition = pair.indexOf(':');
			if (colonPosition < 0) {
				throw new Error(`Parameter ${pair} does not include a colon.`);
			}
			const facet = pair.substr(0, colonPosition);
			const value = pair.substr(colonPosition + 1);
			if (value.charAt(0) !== '"' || value.charAt(value.length - 1) !== '"') {
				throw new Error(`Term ${value} is not bounded by double quotes`);
			}
			return { facet, term: value.slice(1, -1) };
		});
};

export const parseSearchParameter = (search: string) => {
	const query = queryString.parse(search);
	return query.q;
};

const formatFilter = (filter: IFilter) => {
	const term = filter.term && filter.term.replace(/([\\"])/g, '\\$1');
	return `${filter.facet}:"${term}"`;
};

export const containsFilter = (filters: IFilter[], filter: IFilter) =>
	filters.some(item => item.facet === filter.facet && item.term === filter.term);

export const stringifyFilters = (filters: IFilter[]) => {
	if (filters && filters.length) {
		return filters.map(formatFilter).join(',');
	}
	return null;
};

const omitEmptyValues = (obj: object) => {
	const resultObject = {};
	Object.keys(obj).forEach((key) => {
		const value = obj[key];
		if (value) {
			resultObject[key] = value;
		}
	});

	return resultObject;
};

export const updateSearch = (search: string, newQuery: object) => {
	const query = queryString.parse(search);
	const queryObject = omitEmptyValues({
		...query,
		...newQuery,
	});

	return queryString.stringify(queryObject);
};
