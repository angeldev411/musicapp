const elements = {};

enum State {
  Loading = 1,
  Loaded
}

const currentState = {};

function initializeElement(element) {
	if (element.initialize) {
		element.initialize();
	} else {
		console.warn('To use `script-async-loader`, `initialize` method is supposed to be defined. Please double-check your component.');
	}
}

export function initializeScript(element, scriptUrl) {
	if (!currentState[scriptUrl]) {
		currentState[scriptUrl] = State.Loading;

		const pluginScriptTag = document.createElement('script');
		pluginScriptTag.src = scriptUrl;
		pluginScriptTag.async = true;
		pluginScriptTag.onload = () => {
			currentState[scriptUrl] = State.Loaded;
			elements[scriptUrl].forEach(eachElement => {
				initializeElement(eachElement);
			});
			elements[scriptUrl] = [];
		};
		const scripts = document.getElementsByTagName('script')[0];
		scripts.parentNode!.insertBefore(pluginScriptTag, scripts);
	}
	if (currentState[scriptUrl] === State.Loaded) {
		initializeElement(element);
	} else {
		elements[scriptUrl] = elements[scriptUrl] || [];
		elements[scriptUrl].push(element);
	}
}
