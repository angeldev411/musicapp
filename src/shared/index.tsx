import * as React from 'react';
import { Provider } from 'react-redux';
import { ISiteApi } from './interfaces';
import Page from './components/page';
import configureStore from './store/configure-store';
import rootSaga from './sagas';
import { navigate } from './routes';

import '../styles/main.less';

export default async function createApplicationAsync(initialState, api: ISiteApi, Router, routerProps) {
	const store = configureStore(initialState);
	const sagaPromise = store.runSaga(rootSaga(api)).done;

	return {
		getView() {
			return (
				<Router {...routerProps}>
					<Provider store={store}>
						<Page />
					</Provider>
				</Router>
			);
		},
		navigate(pathname: string, search: string) {
			navigate(pathname, search, store);
		},
		async serializeStateAsync() {
			store.close();
			await sagaPromise;

			return store.getState();
		},
	};
}
