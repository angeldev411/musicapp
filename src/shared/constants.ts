import { createAsyncActionTypes } from './utils';

export const fetchCurrentUserActionTypes = createAsyncActionTypes('FETCH_CURRENT_USER');
export const signInActionTypes = createAsyncActionTypes('SIGN_IN');
export const signOutActionTypes = createAsyncActionTypes('SIGN_OUT');
export const registerActionTypes = createAsyncActionTypes('REGISTER');
export const forgotPasswordActionTypes = createAsyncActionTypes('FORGOT_PASSWORD');

export const fetchSearchResultsActionTypes = createAsyncActionTypes('FETCH_SEARCH_RESULTS');
export const fetchMoreSearchResultsActionTypes = createAsyncActionTypes('FETCH_MORE_SEARCH_RESULTS');
export const fetchSongDetailsActionTypes = createAsyncActionTypes('FETCH_SONG_DETAILS');
export const getSitesActionTypes = createAsyncActionTypes('GET_SITES');
export const fetchSearchFieldsActionTypes = createAsyncActionTypes('FETCH_SEARCH_FIELDS');

export const updateFacetsActionType = 'UPDATE_FACETS';
export const updateUserMetadataActionType = createAsyncActionTypes('UPDATE_USER_METADATA');
export const updateDetailsPaneVisibilityActionType = 'UPDATE_DETAILS_PANE_VISIBILITY';

export const GET = 'GET';
export const POST = 'POST';
export const DELETE = 'DELETE';

export const facets = [ 'lists.name', 'preachingThemes.string', 'composers.name', 'meter', 'curated' ];
export const sortDefault = 'lists.rank:asc';
export const sortOptions = [
	{ value: 'lists.rank:asc', label: 'Most Popular' },
	{ value: 'views:desc', label: 'Views' },
	{ value: 'title:asc', label: 'Title' },
	{ value: 'modified:desc', label: 'Last Modified' },
	{ value: 'rating:desc', label: 'User Rating' },
];
