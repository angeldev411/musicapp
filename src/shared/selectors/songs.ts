export const selectSongs = (state) => state.songs;
export const selectFacets = (state) => state.songs.details.facets;
export const selectFacetsTotal = (state) => state.songs.details.total;

export const selectCurrentSong = (state) => state.songs.currentSong;

export const selectSongBySlug = (state, slug) => {
	const songs = state.songs.details.hits;
	const filteredSong = songs.filter(({ hit }) => (hit.slug === slug || hit.id === slug));
	return filteredSong[0] ? filteredSong[0].hit : null;
};

export const selectSearchFields = (state) => state.songs.searchFields;
