import { matchPath } from 'react-router';
import queryString from 'query-string';

import {
	onFetchSearchResultsRequested,
	onFetchSongDetailsRequested,
	onGetSitesRequested,
	onFetchSearchFieldsRequested,
} from './actions';
import { selectSongBySlug } from './selectors';

interface IRoute {
 path: string,
 exact?: boolean,
 onEnter: (match: { params: { [key: string]: string } }, search: string, store: any) => void,
}

const convertSearchToApiQuery = (search: string) => {
	const query = queryString.parse(search);
	const { q = '' } = query;
	const filter = query.filter ? query.filter.replace(/,/g, ' ') : '';

	return `${q} ${filter}`;
};

const routes: IRoute[] = [
	{
		path: '/',
		exact: true,
		onEnter: (match, search, store) => store.dispatch(onFetchSearchResultsRequested({
			query: convertSearchToApiQuery(search),
			sort: queryString.parse(search).sort,
		})),
	},
	{
		path: '/songs/:slug',
		exact: true,
		onEnter: (match, search, store) => store.dispatch(onFetchSongDetailsRequested(match.params.slug)),
	},
	{
		path: '/about',
		exact: true,
		onEnter: (match, search, store) => store.dispatch(onGetSitesRequested()),
	},
	{
		path: '/help',
		exact: true,
		onEnter: (match, search, store) => store.dispatch(onFetchSearchFieldsRequested()),
	},
	{
		path: '/test',
		exact: true,
		onEnter: () => {},
	},
];

export function navigate(pathname: string, search: string, store: any) {
	for (const route of routes) {
		const match = matchPath(pathname, route);
		if (match) {
			route.onEnter(match, search, store);
		}
	}
}
