import {
	GET,
	POST,
	DELETE,
	facets,
	sortDefault,
} from '../shared/constants';
import { ISiteApi } from '../shared/interfaces';
import { serializeQueryParameters } from '../shared/utils';

export default function (requestAsync: (url: string, method: string, params?: object) => Promise<any>, baseUrls: { music: string, accounts: string}) {
	return {
		getLinkPreviewsAsync: (song) => Promise.reject(new Error('getLinkPreviewsAync is unsupported.')),

		editSongFieldAsync: (songId, fieldName, fieldValue, revisionId, arrayOptions) => Promise.reject(new Error('editSongFieldAsync is unsupported.')),

		createSongAsync: (song) => Promise.reject(new Error('createSongAsync is unsupported.')),

		getSongDetailsAsync: (slug, revisionId = null) => {
			let url = `${baseUrls.music}songs/${slug}`;
			if (revisionId) {
				url += `&rev=${revisionId}`;
			}
			return requestAsync(url, GET);
		},

		getCurrentUserAsync: () => requestAsync(`${baseUrls.accounts}users/me`, POST),

		getSitesAsync: () => requestAsync(`${baseUrls.music}sites`, GET),

		searchSongAsync: (query, limit = 10, sort = '') => requestAsync(`${baseUrls.music}songs/search?${serializeQueryParameters({
			q: query,
			limit,
			sort,
		})}`, GET),

		trackViewsAsync: (songId) => requestAsync(`${baseUrls.music}songs/${songId}/viewed`, POST),

		updateUserDataAsync: (songId, userData) => requestAsync(`${baseUrls.music}songs/${songId}/meta`, POST, userData),

		getSongRevisionsAsync: (id, afterId, limit) => requestAsync(`${baseUrls.music}songs/${id}/revisions?${serializeQueryParameters({
			afterId,
			limit,
		})}`, GET),

		createUserLinkAsync: (songId, linkData) => requestAsync(`${baseUrls.music}songs/${songId}/userlinks`, POST, linkData),

		deleteUserLinkAsync: (songId, url) => requestAsync(`${baseUrls.music}songs/${songId}/userlinks`, DELETE, {
			url,
		}),

		getUserLinksAsync: (songId) => requestAsync(`${baseUrls.music}songs/${songId}/userlinks`, GET),

		getSearchFieldsAsync: () => requestAsync(`${baseUrls.music}songs/search/fields`, GET),

		doSearchAsync: (query, limit = 25, sort = sortDefault) => requestAsync(`${baseUrls.music}songs/search?${serializeQueryParameters({
			q: query,
			facets: facets.join(','),
			limit,
			sort,
		})}`, GET),

		getMoreResultsAsync: (query, offset, limit = 25, sort = sortDefault) => requestAsync(`${baseUrls.music}songs/search?${serializeQueryParameters({
			q: query,
			facets: facets.join(','),
			limit,
			offset,
			sort,
		})}`, GET),

		getPreviewAsync: (url) => requestAsync(`${baseUrls.music}songs/links/preview?url=${encodeURI(url)}`, GET),
	};
}
