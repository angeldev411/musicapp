import {
	getSitesActionTypes,
} from '../constants';

const initialState = {
	isProcessing: false,
	details: [],
	error: '',
};

const actionHandlers = {
	[getSitesActionTypes.processing]: (state) => ({
		...state,
		isProcessing: true,
	}),
	[getSitesActionTypes.success]: (state, { sites }) => ({
		...state,
		isProcessing: false,
		details: sites,
		error: '',
	}),
	[getSitesActionTypes.failure]: (state, { error }) => ({
		...state,
		isProcessing: false,
		details: [],
		error,
	}),
};

const sites = (state = initialState, { type, ...rest }: { type: string }) => {
	const handler = actionHandlers[type];
	if (!handler) {
		return state;
	}

	return (handler as (state: object, rest: object) => object)(state, rest);
};

export default sites;
