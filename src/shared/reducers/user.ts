import {
	fetchCurrentUserActionTypes,
	signInActionTypes,
	signOutActionTypes,
	registerActionTypes,
	forgotPasswordActionTypes,
} from '../constants';

const initialState = {
	isProcessing: false,
	isInitialized: false,
	details: {
		id: -1,
	},
	error: '',
};

const actionHandlers = {
	[fetchCurrentUserActionTypes.processing]: (state) => ({
		...state,
		isProcessing: true,
	}),
	[fetchCurrentUserActionTypes.success]: (state, { user }) => ({
		...state,
		isProcessing: false,
		isInitialized: true,
		details: user,
		error: '',
	}),
	[fetchCurrentUserActionTypes.failure]: (state, { error }) => ({
		...state,
		isProcessing: false,
		isInitialized: true,
		details: {
			id: -1,
		},
		error,
	}),
	[signInActionTypes.processing]: (state) => ({
		...state,
		isProcessing: true,
	}),
	[signInActionTypes.success]: (state, { user }) => ({
		...state,
		isProcessing: false,
		details: user,
		error: '',
	}),
	[signInActionTypes.failure]: (state, { error }) => ({
		...state,
		isProcessing: false,
		details: {
			id: -1,
		},
		error,
	}),
	[signOutActionTypes.processing]: (state) => ({
		...state,
		isProcessing: true,
	}),
	[signOutActionTypes.success]: (state) => ({
		...state,
		isProcessing: false,
		details: {
			id: -1,
		},
		error: '',
	}),
	[signOutActionTypes.failure]: (state, { error }) => ({
		...state,
		isProcessing: false,
		error,
	}),
	[registerActionTypes.processing]: (state) => ({
		...state,
		isProcessing: true,
	}),
	[registerActionTypes.success]: (state, { user }) => ({
		...state,
		isProcessing: false,
		details: user,
		error: '',
	}),
	[registerActionTypes.failure]: (state, { error }) => ({
		...state,
		isProcessing: false,
		details: {
			id: -1,
		},
		error,
	}),
	[forgotPasswordActionTypes.processing]: (state) => ({
		...state,
		isProcessing: true,
	}),
	[forgotPasswordActionTypes.success]: (state) => ({
		...state,
		isProcessing: false,
		error: '',
	}),
	[forgotPasswordActionTypes.failure]: (state, { error }) => ({
		...state,
		isProcessing: false,
		error,
	}),
};

const user = (state = initialState, { type, ...rest }: { type: string }) => {
	const handler = actionHandlers[type];
	if (!handler) {
		return state;
	}

	return (handler as (state: object, rest: object) => object)(state, rest);
};

export default user;
