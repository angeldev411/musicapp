import {
	updateDetailsPaneVisibilityActionType,
} from '../constants';

const initialState = {
	isDetailsPaneVisible: false,
};

const actionHandlers = {
	[updateDetailsPaneVisibilityActionType]: (state, { isVisible }) => ({
		...state,
		isDetailsPaneVisible: isVisible,
	}),
};

const ui = (state = initialState, { type, ...rest }: { type: string }) => {
	const handler = actionHandlers[type];

	if (!handler) {
		return state;
	}

	return (handler as (state: object, rest: object) => object)(state, rest);
};

export default ui;
