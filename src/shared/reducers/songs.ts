import {
	fetchSearchResultsActionTypes,
	fetchMoreSearchResultsActionTypes,
	updateFacetsActionType,
	fetchSongDetailsActionTypes,
	updateUserMetadataActionType,
	fetchSearchFieldsActionTypes,
} from '../constants';

import { mapToSong } from '../utils';

const initialState = {
	isProcessing: false,
	filter: {
		query: '',
		limit: 25,
		sort: '',
	},
	details: {
		facets: [],
		hits: [],
		total: 0,
	},
	error: '',
	currentSong: {
		isProcessing: false,
		details: null,
		error: '',
	},
	searchFields: {
		isProcessing: false,
		details: [],
		error: '',
	},
};

const actionHandlers = {
	[fetchSearchResultsActionTypes.processing]: (state) => ({
		...state,
		isProcessing: true,
	}),
	[fetchSearchResultsActionTypes.success]: (state, { song, filter }) => ({
		...state,
		filter: {
			...filter,
		},
		isProcessing: false,
		details: {
			...song,
			hits: (song.hits || []).map(({ hit }) => ({
				hit: mapToSong(hit),
			})),
		},
		error: '',
	}),
	[fetchSearchResultsActionTypes.failure]: (state, { error }) => ({
		...state,
		isProcessing: false,
		details: {
			facets: [],
			hits: [],
			total: 0,
		},
		error,
	}),
	[fetchMoreSearchResultsActionTypes.processing]: (state) => ({
		...state,
		isProcessing: true,
	}),
	[fetchMoreSearchResultsActionTypes.success]: (state, { song }) => ({
		...state,
		isProcessing: false,
		filter: {
			...state.filter,
		},
		details: {
			...song,
			hits: state.details.hits.concat((song.hits || []).map(({ hit }) => ({
				hit: mapToSong(hit),
			}))),
		},
		error: '',
	}),
	[fetchMoreSearchResultsActionTypes.failure]: (state, { error }) => ({
		...state,
		isProcessing: false,
		details: {
			facets: [],
			hits: [],
			total: 0,
		},
		error,
	}),
	[updateFacetsActionType]: (state, { facets }) => ({
		...state,
		details: {
			...state.details,
			facets,
		},
	}),
	[fetchSongDetailsActionTypes.processing]: (state) => ({
		...state,
		currentSong: {
			isProcessing: true,
			details: null,
			error: '',
		},
	}),
	[fetchSongDetailsActionTypes.success]: (state, { song }) => ({
		...state,
		currentSong: {
			isProcessing: false,
			details: song,
		},
	}),
	[fetchSongDetailsActionTypes.failure]: (state, { error }) => ({
		...state,
		currentSong: {
			isProcessing: false,
			details: null,
			error,
		},
	}),
	[updateUserMetadataActionType.success]: (state, { songId, songDetails }) => {
		if (songId !== state.currentSong.details.id) {
			return state;
		}
		return {
			...state,
			currentSong: {
				...state.currentSong,
				details: {
					...state.currentSong.details,
					appData: songDetails.appData,
					userData: songDetails.userData,
				},
			},
		};
	},
	[fetchSearchFieldsActionTypes.processing]: (state) => ({
		...state,
		searchFields: {
			isProcessing: true,
		},
	}),
	[fetchSearchFieldsActionTypes.success]: (state, { fields }) => ({
		...state,
		searchFields: {
			isProcessing: false,
			details: fields,
			error: '',
		},
	}),
	[fetchSearchFieldsActionTypes.failure]: (state, { error }) => ({
		...state,
		searchFields: {
			isProcessing: false,
			error,
		},
	}),
};

const songs = (state = initialState, { type, ...rest }: { type: string }) => {
	const handler = actionHandlers[type];
	if (!handler) {
		return state;
	}

	return (handler as (state: object, rest: object) => object)(state, rest);
};

export default songs;
