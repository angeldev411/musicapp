import { combineReducers } from 'redux';

import sites from './sites';
import user from './user';
import songs from './songs';
import ui from './ui';

const rootReducer = combineReducers({
	sites,
	user,
	songs,
	ui,
});

export default rootReducer;
