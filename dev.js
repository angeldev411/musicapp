require.extensions['.css'] = () => {};
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const serverConfig = require('./webpack.server.config');
const browserConfig = require('./webpack.browser.config');

serverConfig.devtool = 'source-map';
serverConfig.plugins.push(new webpack.HotModuleReplacementPlugin());

browserConfig.devtool = 'source-map';
browserConfig.entry.unshift('webpack/hot/dev-server', 'webpack-dev-server/client?http://localhost:3000');

browserConfig.plugins.push(new webpack.HotModuleReplacementPlugin());

const serverCompiler = webpack(serverConfig);
const browserCompiler = webpack(browserConfig);

let reloadAsync = null;
let serverPromise = Promise.resolve();
function initializeServer() {
	serverPromise
		.then(serverInstance => {
			if (serverInstance) {
				serverInstance.close();
			}

			try {
				delete require.cache[require.resolve('./dist/server')];

				// eslint-disable-next-line global-require,import/no-unresolved
				const server = require('./dist/server');

				serverPromise = server.startAsync(browserCompiler);
				reloadAsync = server.reloadAsync;
			} catch (error) {
				console.log(error && (error.stack || error));
			}
		});
}

const serverDistFolder = path.resolve(__dirname, 'dist', 'server');
try {
	for (const file of fs.readdirSync(serverDistFolder)) {
		fs.unlinkSync(path.resolve(serverDistFolder, file));
	}
} catch (e) {
	// The most likely cause of failure is the dist folder not existing.
}

let assets = new Set();

serverCompiler.watch({ ignored: /node_modules/ }, (error, stats) => {
	if (error) {
		console.error(error.stack || error);
		if (error.details) {
			console.error(error.details);
		}
		return;
	}

	const info = stats.toJson();

	if (stats.hasErrors() || stats.hasWarnings()) {
		for (const webpackError of info.errors || []) {
			console.error(webpackError);
		}
		for (const warning of info.warnings || []) {
			console.warn(warning);
		}

		return;
	}

	if (!reloadAsync) {
		initializeServer();
	} else {
		reloadAsync()
			.catch(() => {
				console.error('Hot module replacement encountered a fatal error. Restarting server.');
				initializeServer();
			})
			.then(() => {
				const newAssets = new Set();
				for (const asset of info.assets) {
					newAssets.add(asset.name);
				}

				for (const file of assets) {
					if (!newAssets.has(file)) {
						fs.unlinkSync(path.resolve(serverDistFolder, file));
					}
				}

				assets = newAssets;
			});
	}
});
