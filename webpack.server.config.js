const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const awesomeTypescriptLoader = require('awesome-typescript-loader');

module.exports = {
	entry: './src/server/index.ts',
	output: {
		path: path.resolve(__dirname, 'dist', 'server'),
		filename: 'index.js',
		libraryTarget: 'commonjs2',
	},
	externals: [ nodeExternals({ whitelist: [ /\.css$/ ] }) ],
	target: 'node',
	node: {
		__dirname: true,
	},
	resolve: {
		extensions: [ '*', '.ts', '.tsx', '.js' ],
	},
	module: {
		loaders: [
			{
				test: /\.css$|\.less$/,
				loader: 'ignore-loader',
			},
			{
				test: /\.tsx?/,
				loader: 'awesome-typescript-loader',
			},
		],
	},
	plugins: [
		new webpack.NamedModulesPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new awesomeTypescriptLoader.CheckerPlugin(),
	],
};
