# Faithlife Music

[![Build Status](https://travis-ci.com/Faithlife/FaithlifeMusic.svg?token=R69fBuXe29TEr5GfePfH&branch=master)](https://travis-ci.com/Faithlife/FaithlifeMusic)

This website provides access to Faithilfe's curated set of songs and resources for them.

## Setup

### Prerequisites

* [Node](https://nodejs.org/en/) (go with "latest" for the best debugging experience)
* [Yarn](https://yarnpkg.com/en/)
* [Visual Studio Code](https://code.visualstudio.com/)—this isn't strictly necessary, but there repository is configured to provide a great out-of-the-box debugging experience if you're using VS Code.
* [nvm](https://github.com/creationix/nvm) (if using VS Code on Mac or Linux—although I have no idea whether this project even runs on Windows…)

After installing the prerequisites, clone this repo and run `yarn install` from the repository root.

Open Visual Studio Code and install all of the "Workspace Recommended Extensions".
When you open the workspace, you should be prompted to install these; if not, open the extensions pane (Command-Shift-X), click on the ellipsis at the top of the panel, and select "Show Workspace Recommended Extensions".

### Building the code

1. Open the repository root in Visual Studio Code.
1. Press `Command-Shift-B`.

Alternatively, instead of opening Visual Studio Code, you can run `yarn build` from the command line.

### Running the code

1. Open the repository root in Visual Studio Code.
1. Press `F5`.

Both the server and the client support Hot Module Replacement when running in dev mode, so once you've got a running server, you can continue to edit the code, and any changes should show up without having to restart the server or even refresh your web browser.

Some files do not support Hot Module Replacement.
In particular, modifications to `dev.js`, `webpack.browser.config.js`, and `webpack.server.config.js` will require a full restart to take effect.
Additionally, adding/removing files or npm packages while running webpack may not work.

If you don't want to run Visual Studio Code, you can run `yarn dev` from the command line instead.
However, running within Visual Studio Code will allow you to attach the Node debugger to the process, which can be quite handy.

*NOTE:*
Visual Studio Code will sometimes prevent you from setting new breakpoints in files that are modified while the debugger is attached.
If you're in this state and you *really* need to set a breakpoint, you can add a `debugger;` statement to the code itself.
Upon reaching this statement, VS Code will open up a read only copy of the latest code and let you step through it.

## Original Site

Faithilfe Music was originally written as an Aurelia application, and the code can be found in [Faithlife/FaithlifeMusicOriginal](https://github.com/Faithlife/FaithlifeMusicOriginal).
Feel free to take anything of value (especially API clients and models) from this repository.

## Coding Guidelines

### General

Follow the eslint rules, and make sure that your code passes `yarn lint` before opening any Pull Requests.

If you run `yarn lint` and discover a bunch of whitespace issues, many of them can be fixed automatically by running `yarn lint -- --fix`.

### Commits

The first line of a commit message should be structured so that it finishes the sentence "This commit will …". For example, a good commit message might be

> Improve error messages.

This includes using the correct tense and mood for verbs. For example, the message

> Improved error message.

is *not* acceptable because "This commit will improved error message" is ungrammatical.

The first line of a commit message should use sentence case (that is, begin your message with a capital letter) and include a period at the end.

These rules also apply to the naming of Pull Requests, as those may eventually become commits.

### Naming Conventions

Events exposed as `Component` props should have an `on` prefix and be expressed in present tense; e.g., `onRequestSignIn`.

Event *handlers* should have a `handle` prefix and be named to the events they are handling; e.g., `handleRequestSignIn` or `handlePasswordChange`.

With the exception of event handlers and method overloads (e.g., `componentDidMount`), any function that returns a `Promise` should have an `Async` suffix.

### React event handlers

One of our `eslint` rules prevents the calling `.bind` on functions within the `render()` method of components.
This also includes declaring arrow functions inside of `render()` that access `this`.

The reason for this is that it causes extra allocations every time the `render()` method is called.

The easiest workaround is to declare your event handlers in the class using the form

```
handleClick: event => {
	// code goes here.
}
```

This will cause the function to be bound at object construction time, and you can reference it in the `render()` method without calling `.bind` on it.
