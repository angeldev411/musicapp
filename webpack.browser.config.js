const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: [
		// babel-polyfill supports Promise on IE/Edge
		'babel-polyfill',
		'whatwg-fetch',
		'./src/browser/index.tsx',
	],
	output: {
		path: path.resolve(__dirname, 'dist', 'browser'),
		publicPath: '/dist/',
		filename: 'index.js',
		libraryTarget: 'var',
	},
	target: 'web',
	resolve: {
		extensions: [ '*', '.ts', '.tsx', '.js' ],
	},
	module: {
		loaders: [
			{
				test: /\.tsx?/,
				loader: 'awesome-typescript-loader',
				options: {
					transpileOnly: true,
				},
			},
			{
				test: /\.eot$|\.ttf$|\.svg$|\.woff$|\.woff2$/,
				loader: 'ignore-loader',
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({ use: [ 'css-loader' ] }),
			},
			{
				test: /\.less$/,
				use: ExtractTextPlugin.extract({ use: [ 'css-loader?url=false', 'less-loader' ] }),
			},
		],
	},
	plugins: [
		new webpack.NamedModulesPlugin(),
		new ExtractTextPlugin({ filename: 'styles.css' }),
	],
};
