require.extensions['.css'] = () => {};
// eslint-disable-next-line import/no-unresolved
const server = require('./dist/server');

server.startAsync()
	.catch(error => console.error(error && (error.stack || error)));
