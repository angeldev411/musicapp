const webpack = require('webpack');
const baseServerConfig = require('./webpack.server.config');
const baseBrowserConfig = require('./webpack.browser.config');

baseBrowserConfig.plugins.push(
	new webpack.optimize.CommonsChunkPlugin({
		name: [ 'vendor' ],
		children: true,
		minChunks: 2,
		async: true,
	}),
	new webpack.LoaderOptionsPlugin({
		minimize: true,
	}),
	new webpack.optimize.UglifyJsPlugin({
		warnings: false,
		drop_console: true,
		drop_debugger: true,
		dead_code: true,
	}),
	new webpack.DefinePlugin({
		'process.env.NODE_ENV': '"production"',
	})
);

baseServerConfig.plugins.push(
	new webpack.LoaderOptionsPlugin({
		minimize: true,
	}),
	new webpack.optimize.UglifyJsPlugin({
		warnings: false,
		drop_console: true,
		drop_debugger: true,
		dead_code: true,
	}),
	new webpack.DefinePlugin({
		'process.env.NODE_ENV': '"production"',
	})
);

module.exports = [ baseServerConfig, baseBrowserConfig ];
